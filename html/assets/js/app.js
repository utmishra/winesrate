const app = (() => {
  // private functions

  function debounce(fn, duration) {
    var timer;
    return function() {
      clearTimeout(timer);
      timer = setTimeout(fn, duration);
    }
  }

  function generateHeader() {
    var myHeaders = new Headers({
      "Content-Type": "application/json",
      "Cache-Control": "no-cache"
    });
  }
  const fetchData = (requestUrl, methodType, requestPayLoad) => {
    var headers = generateHeader();
    var myInit = {
      method: methodType,
      headers: headers,
      mode: 'cors',
      cache: 'default',
      body: requestPayLoad
    };
    fetch(requestUrl, myInit).then(function(response) {
      return response.blob();
    });
  }

  const getAutoSuggestData = (textBox) => {
    var payLoad = {};
    if (textBox.value.length >= 3) {
      payLoad.query = textBox.value;
      payLoad.sortOrder = "ASC";
      payLoad.sortBy = "price";
      payLoad.pageNumber = "0";
      payLoad.size = "20";
      payLoad.continent = "World";
      fetchData("http://132.148.88.175:8080/wines/search", "POST", payLoad)
    }
  }

  const fetchSearchData = () => {
    $(".selectOptions").change(function(){
      $(this).val();
      alert($(this).val());
    });
  }

  // public functions
  const init = () => {
    document.getElementById("serachTextBox").addEventListener("keyup", debounce(() => {
      getAutoSuggestData(document.getElementById("serachTextBox"));
    }, 500));
  }

  return {
    init: init
  }

})();

document.addEventListener("DOMContentLoaded", function(){
  app.init();
});
