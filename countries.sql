-- MySQL dump 10.13  Distrib 5.7.18, for Linux (x86_64)
--
-- Host: localhost    Database: w
-- ------------------------------------------------------
-- Server version	5.7.18-0ubuntu0.16.10.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `countries`
--

DROP TABLE IF EXISTS `countries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `countries` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `country_name` varchar(45) NOT NULL DEFAULT '',
  `continent_name` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=251 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `countries`
--

LOCK TABLES `countries` WRITE;
/*!40000 ALTER TABLE `countries` DISABLE KEYS */;
INSERT INTO `countries` VALUES (1,'Andorra','Europe'),(2,'United Arab Emirates','Asia'),(3,'Afghanistan','Asia'),(4,'Antigua and Barbuda','North America'),(5,'Anguilla','North America'),(6,'Albania','Europe'),(7,'Armenia','Asia'),(8,'Angola','Africa'),(9,'Antarctica','Antarctica'),(10,'Argentina','South America'),(11,'American Samoa','Oceania'),(12,'Austria','Europe'),(13,'Australia','Oceania'),(14,'Aruba','North America'),(15,'Åland','Europe'),(16,'Azerbaijan','Asia'),(17,'Bosnia and Herzegovina','Europe'),(18,'Barbados','North America'),(19,'Bangladesh','Asia'),(20,'Belgium','Europe'),(21,'Burkina Faso','Africa'),(22,'Bulgaria','Europe'),(23,'Bahrain','Asia'),(24,'Burundi','Africa'),(25,'Benin','Africa'),(26,'Saint Barthélemy','North America'),(27,'Bermuda','North America'),(28,'Brunei','Asia'),(29,'Bolivia','South America'),(30,'Bonaire','North America'),(31,'Brazil','South America'),(32,'Bahamas','North America'),(33,'Bhutan','Asia'),(34,'Bouvet Island','Antarctica'),(35,'Botswana','Africa'),(36,'Belarus','Europe'),(37,'Belize','North America'),(38,'Canada','North America'),(39,'Cocos [Keeling] Islands','Asia'),(40,'Democratic Republic of the Congo','Africa'),(41,'Central African Republic','Africa'),(42,'Republic of the Congo','Africa'),(43,'Switzerland','Europe'),(44,'Ivory Coast','Africa'),(45,'Cook Islands','Oceania'),(46,'Chile','South America'),(47,'Cameroon','Africa'),(48,'China','Asia'),(49,'Colombia','South America'),(50,'Costa Rica','North America'),(51,'Cuba','North America'),(52,'Cape Verde','Africa'),(53,'Curacao','North America'),(54,'Christmas Island','Oceania'),(55,'Cyprus','Europe'),(56,'Czechia','Europe'),(57,'Germany','Europe'),(58,'Djibouti','Africa'),(59,'Denmark','Europe'),(60,'Dominica','North America'),(61,'Dominican Republic','North America'),(62,'Algeria','Africa'),(63,'Ecuador','South America'),(64,'Estonia','Europe'),(65,'Egypt','Africa'),(66,'Western Sahara','Africa'),(67,'Eritrea','Africa'),(68,'Spain','Europe'),(69,'Ethiopia','Africa'),(70,'Finland','Europe'),(71,'Fiji','Oceania'),(72,'Falkland Islands','South America'),(73,'Micronesia','Oceania'),(74,'Faroe Islands','Europe'),(75,'France','Europe'),(76,'Gabon','Africa'),(77,'United Kingdom','Europe'),(78,'Grenada','North America'),(79,'Georgia','Asia'),(80,'French Guiana','South America'),(81,'Guernsey','Europe'),(82,'Ghana','Africa'),(83,'Gibraltar','Europe'),(84,'Greenland','North America'),(85,'Gambia','Africa'),(86,'Guinea','Africa'),(87,'Guadeloupe','North America'),(88,'Equatorial Guinea','Africa'),(89,'Greece','Europe'),(90,'South Georgia and the South Sandwich Islands','Antarctica'),(91,'Guatemala','North America'),(92,'Guam','Oceania'),(93,'Guinea-Bissau','Africa'),(94,'Guyana','South America'),(95,'Hong Kong','Asia'),(96,'Heard Island and McDonald Islands','Antarctica'),(97,'Honduras','North America'),(98,'Croatia','Europe'),(99,'Haiti','North America'),(100,'Hungary','Europe'),(101,'Indonesia','Asia'),(102,'Ireland','Europe'),(103,'Israel','Asia'),(104,'Isle of Man','Europe'),(105,'India','Asia'),(106,'British Indian Ocean Territory','Asia'),(107,'Iraq','Asia'),(108,'Iran','Asia'),(109,'Iceland','Europe'),(110,'Italy','Europe'),(111,'Jersey','Europe'),(112,'Jamaica','North America'),(113,'Jordan','Asia'),(114,'Japan','Asia'),(115,'Kenya','Africa'),(116,'Kyrgyzstan','Asia'),(117,'Cambodia','Asia'),(118,'Kiribati','Oceania'),(119,'Comoros','Africa'),(120,'Saint Kitts and Nevis','North America'),(121,'North Korea','Asia'),(122,'South Korea','Asia'),(123,'Kuwait','Asia'),(124,'Cayman Islands','North America'),(125,'Kazakhstan','Asia'),(126,'Laos','Asia'),(127,'Lebanon','Asia'),(128,'Saint Lucia','North America'),(129,'Liechtenstein','Europe'),(130,'Sri Lanka','Asia'),(131,'Liberia','Africa'),(132,'Lesotho','Africa'),(133,'Lithuania','Europe'),(134,'Luxembourg','Europe'),(135,'Latvia','Europe'),(136,'Libya','Africa'),(137,'Morocco','Africa'),(138,'Monaco','Europe'),(139,'Moldova','Europe'),(140,'Montenegro','Europe'),(141,'Saint Martin','North America'),(142,'Madagascar','Africa'),(143,'Marshall Islands','Oceania'),(144,'Macedonia','Europe'),(145,'Mali','Africa'),(146,'Myanmar [Burma]','Asia'),(147,'Mongolia','Asia'),(148,'Macao','Asia'),(149,'Northern Mariana Islands','Oceania'),(150,'Martinique','North America'),(151,'Mauritania','Africa'),(152,'Montserrat','North America'),(153,'Malta','Europe'),(154,'Mauritius','Africa'),(155,'Maldives','Asia'),(156,'Malawi','Africa'),(157,'Mexico','North America'),(158,'Malaysia','Asia'),(159,'Mozambique','Africa'),(160,'Namibia','Africa'),(161,'New Caledonia','Oceania'),(162,'Niger','Africa'),(163,'Norfolk Island','Oceania'),(164,'Nigeria','Africa'),(165,'Nicaragua','North America'),(166,'Netherlands','Europe'),(167,'Norway','Europe'),(168,'Nepal','Asia'),(169,'Nauru','Oceania'),(170,'Niue','Oceania'),(171,'New Zealand','Oceania'),(172,'Oman','Asia'),(173,'Panama','North America'),(174,'Peru','South America'),(175,'French Polynesia','Oceania'),(176,'Papua New Guinea','Oceania'),(177,'Philippines','Asia'),(178,'Pakistan','Asia'),(179,'Poland','Europe'),(180,'Saint Pierre and Miquelon','North America'),(181,'Pitcairn Islands','Oceania'),(182,'Puerto Rico','North America'),(183,'Palestine','Asia'),(184,'Portugal','Europe'),(185,'Palau','Oceania'),(186,'Paraguay','South America'),(187,'Qatar','Asia'),(188,'Réunion','Africa'),(189,'Romania','Europe'),(190,'Serbia','Europe'),(191,'Russia','Europe'),(192,'Rwanda','Africa'),(193,'Saudi Arabia','Asia'),(194,'Solomon Islands','Oceania'),(195,'Seychelles','Africa'),(196,'Sudan','Africa'),(197,'Sweden','Europe'),(198,'Singapore','Asia'),(199,'Saint Helena','Africa'),(200,'Slovenia','Europe'),(201,'Svalbard and Jan Mayen','Europe'),(202,'Slovakia','Europe'),(203,'Sierra Leone','Africa'),(204,'San Marino','Europe'),(205,'Senegal','Africa'),(206,'Somalia','Africa'),(207,'Suriname','South America'),(208,'South Sudan','Africa'),(209,'São Tomé and Príncipe','Africa'),(210,'El Salvador','North America'),(211,'Sint Maarten','North America'),(212,'Syria','Asia'),(213,'Swaziland','Africa'),(214,'Turks and Caicos Islands','North America'),(215,'Chad','Africa'),(216,'French Southern Territories','Antarctica'),(217,'Togo','Africa'),(218,'Thailand','Asia'),(219,'Tajikistan','Asia'),(220,'Tokelau','Oceania'),(221,'East Timor','Oceania'),(222,'Turkmenistan','Asia'),(223,'Tunisia','Africa'),(224,'Tonga','Oceania'),(225,'Turkey','Asia'),(226,'Trinidad and Tobago','North America'),(227,'Tuvalu','Oceania'),(228,'Taiwan','Asia'),(229,'Tanzania','Africa'),(230,'Ukraine','Europe'),(231,'Uganda','Africa'),(232,'U.S. Minor Outlying Islands','Oceania'),(233,'United States','North America'),(234,'Uruguay','South America'),(235,'Uzbekistan','Asia'),(236,'Vatican City','Europe'),(237,'Saint Vincent and the Grenadines','North America'),(238,'Venezuela','South America'),(239,'British Virgin Islands','North America'),(240,'U.S. Virgin Islands','North America'),(241,'Vietnam','Asia'),(242,'Vanuatu','Oceania'),(243,'Wallis and Futuna','Oceania'),(244,'Samoa','Oceania'),(245,'Kosovo','Europe'),(246,'Yemen','Asia'),(247,'Mayotte','Africa'),(248,'South Africa','Africa'),(249,'Zambia','Africa'),(250,'Zimbabwe','Africa');
/*!40000 ALTER TABLE `countries` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-08-21 12:41:36
