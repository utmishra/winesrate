package com.winesrate.web.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.winesrate.web.dtos.BlogDTO;
import com.winesrate.web.dtos.BlogsDTO;
import com.winesrate.web.models.Blog;
import com.winesrate.web.models.Category;
import com.winesrate.web.repository.CategoryRepository;
import com.winesrate.web.services.BlogService;

@Controller
@RequestMapping("/news")
public class NewsController {
	@Autowired
    BlogService blogService;
	
	@Autowired
	CategoryRepository categoryRepository;
	
	@RequestMapping(value = "/create/", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody Blog createImage(@RequestParam("title") String title, 
			@RequestParam("description") String description,
			@RequestParam("content") String content,
			@RequestParam("blogPictureUrl") String blogPictureUrl,
			@RequestParam("categoryId") Integer categoryId) {
		BlogDTO b = new BlogDTO();
		b.setTitle(title);
		b.setDescription(description);
		b.setContent(content);
		b.setBlogUrl(blogPictureUrl);
		b.setCategoryId(5);
    	Blog blog = blogService.createNews(b);
    	return blog;
    }
	
	@RequestMapping(value = "/", method = RequestMethod.GET) 
	public String getBlogs(Model model) {
		BlogsDTO blogs = blogService.findByCategoryId(5, 0, 10, "createdAt", "DESC");
		model.addAttribute("blog", blogs.getBlogs());
		model.addAttribute("pageNumber", 0);
		model.addAttribute("size", 10);
		model.addAttribute("sortBy", "createdAt");
		model.addAttribute("sortOrder", "DESC");
		return "news";
	}
    
    @RequestMapping(value = "/{blogId}", method = RequestMethod.GET)
	public String getSingleBlog(@PathVariable(value = "blogId") Integer blogId,
			Model model) {
		BlogDTO blog = blogService.getBlog(blogId);
		List<Category> categories = categoryRepository.findAll();
		model.addAttribute("categories", categories);
		model.addAttribute("blog", blog);
		return "new";
	}       
}