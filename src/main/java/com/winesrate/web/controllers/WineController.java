package com.winesrate.web.controllers;

import java.util.List;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.winesrate.web.dtos.LocalStores;
import com.winesrate.web.dtos.ResponseDTO;
import com.winesrate.web.dtos.SearchRequest;
import com.winesrate.web.dtos.SearchResponse;
import com.winesrate.web.models.Rating;
import com.winesrate.web.search.models.Wine;
import com.winesrate.web.search.repository.LocalStoreRepository;
import com.winesrate.web.search.services.SearchService;
import com.winesrate.web.services.WineService;

@Controller
@RequestMapping("/wine")
public class WineController {
	
	private static final Logger logger = LoggerFactory.getLogger(WineController.class);
	
	@Autowired 
	private HttpSession httpSession;
	
    @Autowired
    SearchService searchService;
    
    @Autowired
    WineService wineService;
    
    @Autowired 
    LocalStoreRepository localStoreRepository;
	
    @RequestMapping(value = "/ratings/{wineId}", method = RequestMethod.POST, produces = "application/json", consumes = "application/json")
    public @ResponseBody Rating saveRating(@PathVariable("wineId") Integer wineId, @RequestBody Rating requestRating) {
    	requestRating.setWineId(wineId);
    	Rating savedRating = wineService.addRating(requestRating);
    	return savedRating;
    }
    
    @RequestMapping(value = "/local-store", method = RequestMethod.POST, produces = "application/json")
    public @ResponseBody ResponseDTO addLocalStore(@RequestBody(required = true) @Valid LocalStores store) {
    	com.winesrate.web.search.models.LocalStores ls = new com.winesrate.web.search.models.LocalStores();
    	ls.setDist(store.getDist());
    	ls.setGroup(store.getGroup());
    	ls.setManager(store.getManager());
    	ls.setSupv(store.getSupv());
    	ls.setTelephone(store.getTelephone());
    	ls.setType(store.getType());
    	ls.setEmail(store.getEmail());
    	ls.setWebsite(store.getWebsite());
    	ls.setStoreLocation(store.getStoreLocation());
    	ResponseDTO response = new ResponseDTO();
    	try {
    		ls = localStoreRepository.save(ls);
    		logger.info(ls.toString());
    	}
    	catch(Exception e) {
    		response.setStatus(500);
    		response.setMessage("Something went wrong! Error: " + e.getMessage());  
    		return response;
    	}
    	response.setMessage("Done");
    	response.setStatus(200);
    	return response;
    	
    }
    
    @RequestMapping(value = "/local-store", method = RequestMethod.GET)
    public String loadLocalStoreForm(Model model) {
    	return "localstore";
    }
    
    @RequestMapping(value = "/{query}", method = RequestMethod.GET)
	public String searchWines(@PathVariable(value = "query") String query, Model model, HttpSession httpSession) {
		List<com.winesrate.web.search.models.Wine> resultWines = searchService.singleWine(query);
		model.addAttribute("query", query);
		model.addAttribute("wines", resultWines);
		return "wine";
	}
    
    @RequestMapping(value = "/", method = RequestMethod.POST)
	public String searchWinesPost(@RequestParam(value = "query") String query, 
				@RequestParam(value = "continent", defaultValue = "") String continent, 
				@RequestParam(value = "vintage", defaultValue = "") Integer vintage, 
				@RequestParam(value = "sortBy", defaultValue = "price") String sortBy,
				@RequestParam(value = "sortOrder", defaultValue = "asc") String sortOrder,
				@RequestParam(value = "pageNumber", defaultValue = "0") Integer pageNumber,
				@RequestParam(value = "size", defaultValue = "10") Integer size,
				Model model,
				HttpSession httpSession) {    	    	
    	
    	logger.info("Query = " + query);
    	logger.info("Continent = " + continent);
    	logger.info("Vintage = " + vintage);
    	logger.info("Sort by = " + sortBy);
    	logger.info("Sort Order = " + sortOrder);    	    	
    	
    	SearchRequest request = new SearchRequest();
    	List<Wine> recommended = null;
    	request.setQuery(query);
    	request.setContinent(continent);
    	request.setVintage(vintage);
    	request.setPageNumber(pageNumber);
    	request.setSize(size);
    	request.setSortBy(sortBy != "" ? sortBy : "price");
    	request.setSortOrder(sortOrder != "" ? sortOrder : "asc");
    	SearchResponse resultWines = searchService.searchWines(request, httpSession.getId());    
    	if(resultWines.getWines().size() == 0) {
    		recommended = searchService.recommendedWines().getWines();
    	}
    	List<LocalStores> ls = searchService.getLocalStores();  
    	
    	model.addAttribute("localStores", ls);
		model.addAttribute("query", request.getQuery());
		model.addAttribute("wines", resultWines.getWines());
		model.addAttribute("aggs", resultWines.getAggregatations().get(0).getBuckets());
		model.addAttribute("continent", request.getContinent());
		model.addAttribute("vintage", resultWines.getAggregatations().get(1).getBuckets());
		model.addAttribute("stores", resultWines.getAggregatations().get(2).getBuckets());
		model.addAttribute("brands", resultWines.getAggregatations().get(3).getBuckets());
		model.addAttribute("countries", resultWines.getAggregatations().get(4).getBuckets());
		model.addAttribute("avgPrice", Math.round(resultWines.getAvgPrice()));
		model.addAttribute("reqVintage", vintage);
		model.addAttribute("totalPages", resultWines.getTotalPages());
		model.addAttribute("currentPage", request.getPageNumber());
		model.addAttribute("firstPageNav", request.getPageNumber() >= 10 ? request.getPageNumber() - (request.getPageNumber() % 10) : 1);
		model.addAttribute("lastPageNav", request.getPageNumber() >= 10 ? (request.getPageNumber()/10 + 1) * 10 : resultWines.getTotalPages() > 10 ? 10 : resultWines.getTotalPages());
		model.addAttribute("sortBy", request.getSortBy());
		model.addAttribute("sortOrder", request.getSortOrder());
		model.addAttribute("recommended", recommended);
		logger.info("totalPages = " + resultWines.getTotalPages());
		logger.info("No. of wines " + resultWines.getWines().size());		
		logger.info("currentPage = " + request.getPageNumber());
		logger.info("firstPageNav = " + (request.getPageNumber() > 10 ? request.getPageNumber() - (request.getPageNumber() % 10) : 1));
//		logger.info("lastPageNav = " + (request.getPageNumber() > 10 ? request.getPageNumber()/10 == resultWines.getTotalPages()/10 ? resultWines.getTotalPages() : (request.getPageNumber()/10 + 1) * 10 : 10));
		
		return "winesearch";
	}
    
    @RequestMapping(value = "/ajax", method = RequestMethod.POST)
	public @ResponseBody SearchResponse searchWinesAjax(@RequestParam(value = "query") String query, 
				@RequestParam(value = "continent", defaultValue = "") String continent, 
				@RequestParam(value = "vintage", defaultValue = "") Integer vintage, 
				@RequestParam(value = "sortBy", defaultValue = "price") String sortBy,
				@RequestParam(value = "sortOrder", defaultValue = "asc") String sortOrder,
				@RequestParam(value = "pageNumber", defaultValue = "0") Integer pageNumber,
				@RequestParam(value = "size", defaultValue = "10") Integer size,
				Model model) {    	    	
    	
    	logger.info("Query = " + query);
    	logger.info("Continent = " + continent);
    	logger.info("Vintage = " + vintage);
    	logger.info("Sort by = " + sortBy);
    	logger.info("Sort Order = " + sortOrder);
    	
    	SearchRequest request = new SearchRequest();
    	request.setQuery(query);
    	request.setContinent(continent);
    	request.setVintage(vintage);
    	request.setPageNumber(pageNumber);
    	request.setSize(size);
    	request.setSortBy(sortBy != "" ? sortBy : "price");
    	request.setSortOrder(sortOrder != "" ? sortOrder : "asc");
    	SearchResponse resultWines = searchService.searchWines(request, httpSession.getId());	
    	if(resultWines.getWines().size() == 0) {
    		resultWines.setRecommended(searchService.recommendedWines().getWines());
    	}
    	resultWines.setQuery(query);
    	resultWines.setCurrentPageNumber(pageNumber);
    	resultWines.setFirstPageNav(request.getPageNumber() >= 10 ? request.getPageNumber() - (request.getPageNumber() % 10) : 1);
    	resultWines.setLastPageNav( request.getPageNumber() >= 10 ? (request.getPageNumber()/10 + 1) * 10 : resultWines.getTotalPages() > 10 ? 10 : resultWines.getTotalPages());
    	resultWines.setSortBy(sortBy);
    	resultWines.setSortOrder(sortOrder);
    	resultWines.setVintage(vintage);
    	resultWines.setContinent(continent);
    	logger.info("No. of wines " + resultWines.getWines().size());
    	logger.info(resultWines.getQuery());    	
		return resultWines;
	}
    
    @RequestMapping(value = "/guide", method = RequestMethod.GET)
	public String wineGuide(Model model) {    	    	    	    	
    	
    	SearchRequest request = new SearchRequest();
    	request.setQuery("");
    	request.setContinent("");
    	request.setVintage(null);
    	request.setPageNumber(0);
    	request.setSize(10);
    	request.setSortBy("price");
    	request.setSortOrder("asc");
    	SearchResponse resultWines = searchService.wineGuide(request);	
    	
		model.addAttribute("query", request.getQuery());
		model.addAttribute("wines", resultWines.getWines());
		model.addAttribute("aggs", resultWines.getAggregatations().get(0).getBuckets());
		model.addAttribute("continent", request.getContinent());
		model.addAttribute("vintage", resultWines.getAggregatations().get(1).getBuckets());
		model.addAttribute("reqVintage", null);
		model.addAttribute("totalPages", resultWines.getTotalPages());
		model.addAttribute("currentPage", request.getPageNumber());
		model.addAttribute("firstPageNav", request.getPageNumber() >= 10 ? request.getPageNumber() - (request.getPageNumber() % 10) : 1);
		model.addAttribute("lastPageNav", request.getPageNumber() >= 10 ? (request.getPageNumber()/10 + 1) * 10 : 10);
		model.addAttribute("sortBy", request.getSortBy());
		model.addAttribute("sortOrder", request.getSortOrder());
		
//		logger.info("totalPages = " + resultWines.getTotalPages());
//		logger.info("currentPage = " + request.getPageNumber());
//		logger.info("firstPageNav = " + (request.getPageNumber() > 10 ? request.getPageNumber() - (request.getPageNumber() % 10) : 1));
//		logger.info("lastPageNav = " + (request.getPageNumber() > 10 ? request.getPageNumber()/10 == resultWines.getTotalPages()/10 ? resultWines.getTotalPages() : (request.getPageNumber()/10 + 1) * 10 : 10));
		
		return "guide";
	}
    
    @RequestMapping(value = "/guide", method = RequestMethod.POST)
	public String wineGuide(@RequestParam(value = "query") String query, 
				@RequestParam(value = "continent", defaultValue = "") String continent, 
				@RequestParam(value = "vintage", defaultValue = "") Integer vintage, 
				@RequestParam(value = "sortBy", defaultValue = "price") String sortBy,
				@RequestParam(value = "sortOrder", defaultValue = "asc") String sortOrder,
				@RequestParam(value = "pageNumber", defaultValue = "0") Integer pageNumber,
				@RequestParam(value = "size", defaultValue = "10") Integer size,
				Model model) {    	    	
    	
    	logger.info("Query = " + query);
    	logger.info("Continent = " + continent);
    	logger.info("Vintage = " + vintage);
    	logger.info("Sort by = " + sortBy);
    	logger.info("Sort Order = " + sortOrder);
    	
    	SearchRequest request = new SearchRequest();
    	request.setQuery(query);
    	request.setContinent(continent);
    	request.setVintage(vintage);
    	request.setPageNumber(pageNumber);
    	request.setSize(size);
    	request.setSortBy(sortBy != "" ? sortBy : "price");
    	request.setSortOrder(sortOrder != "" ? sortOrder : "asc");
    	SearchResponse resultWines = searchService.searchWines(request, httpSession.getId());	
    	
		model.addAttribute("query", request.getQuery());
		model.addAttribute("wines", resultWines.getWines());
		model.addAttribute("aggs", resultWines.getAggregatations().get(0).getBuckets());
		model.addAttribute("continent", request.getContinent());
		model.addAttribute("vintage", resultWines.getAggregatations().get(1).getBuckets());
		model.addAttribute("reqVintage", vintage);
		model.addAttribute("totalPages", resultWines.getTotalPages());
		model.addAttribute("currentPage", request.getPageNumber());
		model.addAttribute("firstPageNav", request.getPageNumber() >= 10 ? request.getPageNumber() - (request.getPageNumber() % 10) : 1);
		model.addAttribute("lastPageNav", request.getPageNumber() >= 10 ? (request.getPageNumber()/10 + 1) * 10 : 10);
		model.addAttribute("sortBy", request.getSortBy());
		model.addAttribute("sortOrder", request.getSortOrder());
		
//		logger.info("totalPages = " + resultWines.getTotalPages());
//		logger.info("currentPage = " + request.getPageNumber());
//		logger.info("firstPageNav = " + (request.getPageNumber() > 10 ? request.getPageNumber() - (request.getPageNumber() % 10) : 1));
//		logger.info("lastPageNav = " + (request.getPageNumber() > 10 ? request.getPageNumber()/10 == resultWines.getTotalPages()/10 ? resultWines.getTotalPages() : (request.getPageNumber()/10 + 1) * 10 : 10));
		
		return "guide";
	}    
}