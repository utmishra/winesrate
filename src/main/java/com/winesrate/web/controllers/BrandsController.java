package com.winesrate.web.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.winesrate.web.models.Brands;
import com.winesrate.web.repository.BrandRepository;

@Controller
@RequestMapping("/brands")
public class BrandsController {
    
    @Autowired
    BrandRepository brandRepository;
    
    @RequestMapping(value = "/", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
    public @ResponseBody Brands addBrand(@RequestBody Brands requestBrand) {
        Brands responseBrand= brandRepository.save(requestBrand);
        return responseBrand;
    }    
}