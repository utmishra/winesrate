package com.winesrate.web.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.winesrate.web.dtos.Wine;
import com.winesrate.web.dtos.WineDTO;
import com.winesrate.web.models.StoreScore;
import com.winesrate.web.models.Wines;
import com.winesrate.web.search.services.SearchService;
import com.winesrate.web.services.WineService;

@Controller
@RequestMapping("/wines")
public class WinesController {
    
    @Autowired
    WineService wineService;
    
    @Autowired
    SearchService searchService;
    
    @RequestMapping(value = "/storeScores", method = RequestMethod.GET)
    public @ResponseBody List<StoreScore> getStoreScores() {
    	return wineService.getStoreScores();
    }
    
    @RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = "application/json")
    public String getWine(@PathVariable("id") Integer wineId, Model model) {
        Wine wine = wineService.getWine(wineId);
        model.addAttribute("wine", wine);
        return "wine";
    }   
    
    @RequestMapping(value = "/", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
    public @ResponseBody Wines addWine(@RequestBody WineDTO requestWine) {
        Wines responseWine = wineService.addWine(requestWine);
        return responseWine;
    }
    
    @RequestMapping(value = "/", method = RequestMethod.PUT, consumes = "application/json", produces = "application/json")
    public @ResponseBody Wines updateWine(@RequestBody WineDTO requestWine) {
        Wines responseWine = wineService.updateWine(requestWine);
        return responseWine;
    }
    
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE, produces = "application/json")
    public String deleteWine(@PathVariable("id") Integer wineId) {
        wineService.deleteWine(wineId);
        return "Deleted";
    }    
}