package com.winesrate.web.controllers;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.winesrate.web.dtos.RecommendedWines;
import com.winesrate.web.dtos.SearchRequest;
import com.winesrate.web.dtos.SearchResponse;
import com.winesrate.web.dtos.Suggestions;
import com.winesrate.web.search.models.Wine;
import com.winesrate.web.search.services.SearchService;

@Controller
@RequestMapping("/wines/search")
public class SearchController {
	
	@Autowired 
	private HttpSession httpSession;
	
	@Autowired
	SearchService searchService;
	
	@RequestMapping(value = "/recommended", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody RecommendedWines recommendedWines() {
		RecommendedWines wines = searchService.recommendedWines();		
		return wines;
	}
	
	@RequestMapping(value = "/{query}", method = RequestMethod.GET, produces = "application/json")
	public String searchWines(@PathVariable("query") String query, Model model) {
		List<Wine> resultWines = searchService.findWines(query);
		model.addAttribute("wines", resultWines);
		model.addAttribute("query", query);
		return "search";
	}
	
	@RequestMapping(value = "/", method = RequestMethod.POST, produces = "application/json")
	public String searchWinesPost(@RequestBody SearchRequest request, Model model, HttpSession httpSession) {
		SearchResponse resultWines = searchService.searchWines(request, httpSession.getId());
		model.addAttribute("query", request.getQuery());
		model.addAttribute("wines", resultWines.getWines());
		return "searchPost";
	}
	
	@RequestMapping(value = "/form", method = RequestMethod.POST, produces = "application/json")
	public String searchWinesPostForm(@ModelAttribute SearchRequest request, Model model) {
		SearchResponse  resultWines = searchService.searchWines(request, httpSession.getId());
		model.addAttribute("query", request.getQuery());
		model.addAttribute("wines", resultWines.getWines());		
		return "searchPost";
	}
	
	@RequestMapping(value = "/suggest/{query}", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ArrayList<Suggestions> suggestWines(@PathVariable("query") String query) {
		ArrayList<Suggestions> suggestions = searchService.suggestWines(query);
		return suggestions;
	}		
}