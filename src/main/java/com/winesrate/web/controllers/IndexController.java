package com.winesrate.web.controllers;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.winesrate.web.search.models.Wine;
import com.winesrate.web.search.services.SearchService;

@Controller
public class IndexController {
	
	@Autowired 
	private HttpSession httpSession;
	
	@Autowired
    SearchService searchService;
	
    @RequestMapping(value = "/", method = RequestMethod.GET)
	public String index(Model model) {    	
		return "index";
	}
    
    @RequestMapping(value = "/about", method = RequestMethod.GET)
   	public String about(Model model) {       	
   		return "about";
   	}
    
    @RequestMapping(value = "/news", method = RequestMethod.GET)
   	public String news(Model model) {
       	List<Wine> recommendedWines = searchService.recommendedWines().getWines();
       	model.addAttribute("recommended", recommendedWines);
   		return "news";
   	}
    
    @RequestMapping(value = "/about-us", method = RequestMethod.GET)
   	public String aboutUs(Model model) {
       	List<Wine> recommendedWines = searchService.recommendedWines().getWines();
   		return "about-us";
   	}
}
