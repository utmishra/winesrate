
package com.winesrate.web.controllers;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.sql.Timestamp;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.winesrate.web.dtos.ImageDTO;
import com.winesrate.web.models.Image;
import com.winesrate.web.services.ImageService;

@Controller
@RequestMapping("/image")
@PropertySource(value = "classpath:application.properties")
public class ImageController {
	
	private static final Logger logger = LoggerFactory.getLogger(ImageController.class);

	@Autowired
    ImageService imageService;
	
	@Resource
    private Environment environment;
	
    @RequestMapping(value = "/{imageId}", method = RequestMethod.GET)
	public  @ResponseBody Image getImage(@PathVariable(value = "imageId") Integer imageId) {
		Image image = imageService.getImage(imageId);
		return image;
	}
    
    @RequestMapping(value = "/create/", method = RequestMethod.POST)
	public @ResponseBody Image createImage(@RequestParam("image") MultipartFile image, @RequestParam("imageType") String imageType, @RequestHeader String host) {
    	String path = environment.getProperty("winesrate.blogImageDir");
        String fileName = image.getOriginalFilename();        
        long timestamp = new Timestamp(System.currentTimeMillis()).getTime();
        String fullPath = path + timestamp + fileName;
        Image response = null;
        try {  
	        byte barr[] = image.getBytes();  	          
	        BufferedOutputStream bout = new BufferedOutputStream(new FileOutputStream(fullPath));  
	        bout.write(barr);  
	        bout.flush();  
	        bout.close();            
	        logger.info("Image path = " + "http://" + host + "/images/blog/" + timestamp + fileName);
	        logger.info("Full path = " + fullPath);
	        ImageDTO imageRequest = new ImageDTO();
	        imageRequest.setImageType(imageType);
	        imageRequest.setImageURL("http://" + host + "/images/blog/" + timestamp + fileName);
	        File f = new File(fullPath);
	        response = imageService.createImage(imageRequest);
        }
        catch(Exception e) {
        	System.out.println(e);          	
        }
        return response;
    }
}