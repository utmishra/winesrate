package com.winesrate.web.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.winesrate.web.models.Stores;
import com.winesrate.web.repository.StoresRepository;

@Controller
@RequestMapping("/stores")
public class StoreController {
    
    @Autowired
    StoresRepository storeRepository;
    
    @RequestMapping(value = "/", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
    public @ResponseBody Stores addStore(@RequestBody Stores requestStore) {
        Stores responseStore = storeRepository.save(requestStore);
        return responseStore;
    }    
}
