package com.winesrate.web.controllers;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.sql.Timestamp;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.winesrate.web.models.Category;
import com.winesrate.web.repository.CategoryRepository;
import com.winesrate.web.search.services.BulkIndexService;
import com.winesrate.web.services.AdminService;

@Controller
@RequestMapping("/admin")
@PropertySource(value = "classpath:application.properties")
public class AdminController {    
	
    @Resource
    private Environment environment;
	
	@Autowired
	AdminService adminService;
	
	@Autowired 
	CategoryRepository categoryRepository;
	
	@Autowired
	BulkIndexService bulkIndexService;
    
	@RequestMapping(value="/uploads/csv", method=RequestMethod.POST, produces = "application/json")  
	public @ResponseBody ResponseEntity<?> upload(@RequestParam("file") MultipartFile file, @RequestParam("storeName") String storeName, @RequestParam("storeURL") String storeURL, @RequestParam("logoURL") String logoURL) {  
        String path = environment.getProperty("winesrate.uploads");
        String fileName = file.getOriginalFilename();
        long timestamp = new Timestamp(System.currentTimeMillis()).getTime();
        String fullPath = path + "csv/" + fileName + '.' +  timestamp;
        
        try{  
	        byte barr[] = file.getBytes();  	          
	        BufferedOutputStream bout = new BufferedOutputStream(new FileOutputStream(fullPath));  
	        bout.write(barr);  
	        bout.flush();  
	        bout.close();            
	        File f = new File(fullPath);
	        adminService.indexCSV(f, storeName, storeURL, logoURL);
        }
        catch(Exception e) {
        	System.out.println(e);
        }
        return new ResponseEntity<Object>(HttpStatus.CREATED);
	}  
	
	@RequestMapping(value="/uploads/csv", method=RequestMethod.GET) 
	public String uploadForm() {
		return "admin/upload";
	}
	
	@RequestMapping(value = "/blogs/create", method=RequestMethod.GET)
	public String createBlog(Model model) {
		List<Category> categories = categoryRepository.findAll();
		model.addAttribute("categories", categories);
		return "admin/create-blog";
	}
	
	@RequestMapping(value = "/news/create", method=RequestMethod.GET)
	public String createNews(Model model) {
		List<Category> categories = categoryRepository.findAll();
		model.addAttribute("categories", categories);
		return "admin/create-news";
	}
	
	@RequestMapping(value = "/bulk/index-scores", method=RequestMethod.POST)
	public @ResponseBody String indexBulkScores() {
		return bulkIndexService.indexScores();
	}
}