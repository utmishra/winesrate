package com.winesrate.web.search.repository;

import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

import com.winesrate.web.search.models.LocalStores;

public interface LocalStoreRepository extends ElasticsearchRepository<LocalStores, Long> {
	
}
