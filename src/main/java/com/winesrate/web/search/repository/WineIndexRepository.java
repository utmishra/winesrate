package com.winesrate.web.search.repository;

import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

import com.winesrate.web.search.models.Wine;

public interface WineIndexRepository  extends ElasticsearchRepository<Wine, Long> {
	
}
