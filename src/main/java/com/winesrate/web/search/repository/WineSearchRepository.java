package com.winesrate.web.search.repository;

import java.util.List;

import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

import com.winesrate.web.search.models.Wine;

public interface WineSearchRepository  extends ElasticsearchRepository<Wine, Long> {
	public List<Wine> findByTitle(String query);
}
