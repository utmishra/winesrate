package com.winesrate.web.search.models;

import java.util.ArrayList;
import java.util.HashMap;

public class SuggestDTO {
	
	public ArrayList<String> input;
	public String output;
	public HashMap<String, String> payload;
	public Integer weight;
	
	public SuggestDTO() {
		this.weight = 0;
	}

	public ArrayList<String> getInput() {
		return input;
	}

	public void setInput(ArrayList<String> input) {
		this.input = input;
	}

	public String getOutput() {
		return output;
	}

	public void setOutput(String output) {
		this.output = output;
	}		

	public HashMap<String, String> getPayload() {
		return payload;
	}

	public void setPayload(HashMap<String, String> payload) {
		this.payload = payload;
	}

	public Integer getWeight() {
		return weight;
	}

	public void setWeight(Integer weight) {
		this.weight = weight;
	}

	@Override
	public String toString() {
		return "SuggestDTO [input=" + input + ", output=" + output + ", payload=" + payload + ", weight=" + weight
				+ "]";
	}
}
