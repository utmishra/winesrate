package com.winesrate.web.search.models;

import java.util.ArrayList;
import java.util.HashMap;

import javax.persistence.Column;

import org.springframework.data.elasticsearch.annotations.CompletionField;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.core.completion.Completion;

import com.winesrate.web.models.Brands;
import com.winesrate.web.models.Stores;

@Document(indexName = "wines", type = "wine")
public class Wine {
    
    private Integer id;
    private String title;
    private String titleNotAnalyzed;
    private String vintage;
    private String country;
    private String continent;
    private String region;
    private String sub_region;
    private String appellation;
    private String color;
    private String bottleSize;
    private Float price;
    private Stores store;
    private String storeId;
    private Brands brand;
    @Column(name = "createdAt", columnDefinition="DATETIME")
    private String createdAt;
    @Column(name = "createdAt", columnDefinition="DATETIME")
    private String updatedAt;
    private String URL;
    private Float rating;
    @CompletionField(maxInputLength = 50, payloads = true)
    private Completion suggest;
    
    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }
    public String getTitle() {
        return title;
    }
    public void setTitle(String title) {
        this.title = title;
        setTitleNotAnalyzed();
    }
	public String getTitleNotAnalyzed() {
		return titleNotAnalyzed;
	}
	public void setTitleNotAnalyzed() {
		this.titleNotAnalyzed = this.title;
	}	
    public String getVintage() {
        return vintage;
    }
    public void setVintage(String vintage) {
        this.vintage = vintage;
    }
    public String getCountry() {
        return country;
    }
    public void setCountry(String country) {
        this.country = country;
    }
    public String getContinent() {
		return continent;
	}
	public void setContinent(String continent) {
		this.continent = continent;
	}
    public String getRegion() {
        return region;
    }
    public void setRegion(String region) {
        this.region = region;
    }
    public String getSub_region() {
        return sub_region;
    }
    public void setSub_region(String sub_region) {
        this.sub_region = sub_region;
    }
    public String getAppellation() {
        return appellation;
    }
    public void setAppellation(String appellation) {
        this.appellation = appellation;
    }
    public String getColor() {
        return color;
    }
    public void setColor(String color) {
        this.color = color;
    }
    public String getBottleSize() {
        return bottleSize;
    }
    public void setBottleSize(String bottleSize) {
        this.bottleSize = bottleSize;
    }
    public Float getPrice() {
        return price;
    }
    public void setPrice(Float price) {
        this.price = price;
    }
    public Stores getStore() {
        return store;
    }
    public void setStore(Stores store) {
        this.store = store;
    }
    public Brands getBrand() {
        return brand;
    }
    public void setBrand(Brands brand) {
        this.brand = brand;
    }
	public String getURL() {
		return URL;
	}
	public void setURL(String uRL) {
		URL = uRL;
	}
	public Completion getSuggest() {
		return suggest;
	}
	public void setSuggest() {
		HashMap<String, String> payload = new HashMap<String, String>();
		payload.put("id", id.toString());
		payload.put("appellation", appellation);
		payload.put("storeName", store.getName());
		suggest = new Completion(generateInput());
		suggest.setOutput(title);
		suggest.setPayload(payload);
	}
	public String[] generateInput() {
		String[] words = title.split("\\s+");
		ArrayList<String> listInput = new ArrayList<String>();
		for(int i = 0; i < words.length; i++) {
		    for(int j = 0; j < words.length; j++) {
		        String prefix = words[i];
		        String currentInput = prefix + " ";
		        for(int k = j; k < words.length; k++) {
    		        if(i != k)
    		            currentInput += words[k] + " ";
		        }
		        listInput.add(currentInput);
		    }
		}
		String[] input = listInput.toArray(new String[listInput.size()]);
		return input;
	}
	public Float getRating() {
		return rating;
	}
	public void setRating(Float rating) {
		this.rating = rating;
	}
	public String getStoreId() {
		return storeId;
	}
	public void setStoreId(String storeId) {
		this.storeId = storeId;
	}	
	@Override
	public String toString() {
		return "Wine [id=" + id + ", title=" + title + ", titleNotAnalyzed=" + titleNotAnalyzed + ", vintage=" + vintage
				+ ", country=" + country + ", continent=" + continent + ", region=" + region + ", sub_region="
				+ sub_region + ", appellation=" + appellation + ", color=" + color + ", bottleSize=" + bottleSize
				+ ", price=" + price + ", store=" + store + ", brand=" + brand + ", createdAt=" + createdAt
				+ ", updatedAt=" + updatedAt + ", URL=" + URL + ", suggest=" + suggest + "]";
	}	
}
