package com.winesrate.web.search.services;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.elasticsearch.action.bulk.BulkRequestBuilder;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.client.Client;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
import org.springframework.data.elasticsearch.core.aggregation.AggregatedPage;
import org.springframework.stereotype.Service;

import static org.elasticsearch.common.xcontent.XContentFactory.*;

import com.winesrate.web.repository.StoresRepository;
import com.winesrate.web.repository.WineRepository;
import com.winesrate.web.search.models.Wine;
import com.winesrate.web.search.repository.WineSearchRepository;

@Service
public class BulkIndexServiceImpl implements BulkIndexService {
	
	private static final Logger logger = LoggerFactory.getLogger(BulkIndexServiceImpl.class);
	
	@Autowired
    WineRepository wineRepository;
	
	@Autowired
	WineSearchRepository wineSearchRepository;
	
	@Autowired
    StoresRepository storeRepository;
	
	@Autowired 
	ElasticsearchTemplate esTemplate;
	
	@Override
	public String indexScores() {			
		Client client = esTemplate.getClient();
		BulkRequestBuilder bulkRequest = client.prepareBulk();
		HashMap<Long, Long> storeScores = new HashMap<Long, Long>();
		List<Object[]> result = wineRepository.getAllStoreCounts();
		for(int i = 0; i < result.size(); i++) {
			storeScores.put(Long.parseLong(result.get(i)[0].toString()), Long.parseLong(result.get(i)[1].toString()));
			logger.info("Store ID = " + result.get(i)[0].toString());
		}
		
		AggregatedPage<Wine> wines = (AggregatedPage<Wine>) wineSearchRepository.findAll();
		Iterator<Wine> wineIterator = wines.iterator();
		while(wineIterator.hasNext()) {
			Wine next = wineIterator.next();
			String storeId = next.getStore().getStoreId().toString();
			logger.info("Score = " + 1 / Math.log(storeScores.get(Long.parseLong(storeId))));			
			String storeScore = new Double(1 / Math.log(storeScores.get(Long.parseLong(storeId)))).toString();
			logger.info("storeScore = " + storeScore);
			try {
				bulkRequest.add(client.prepareUpdate("wines", "wine", next.getId().toString())
				        .setDoc(jsonBuilder()
				                    .startObject()
				                        .field("storeScore", storeScore)
				                    .endObject()
				                  )
				        );
			} catch (Exception e1) {
				e1.printStackTrace();
			}
		}

		BulkResponse bulkResponse = bulkRequest.get();
		if (bulkResponse.hasFailures()) {
		    // process failures by iterating through each bulk response item
		}
		return "Done";
	}
	
}
