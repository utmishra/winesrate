package com.winesrate.web.search.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.common.base.CaseFormat;
import com.winesrate.web.models.Brands;
import com.winesrate.web.models.Country;
import com.winesrate.web.models.Stores;
import com.winesrate.web.models.Wines;
import com.winesrate.web.repository.BrandRepository;
import com.winesrate.web.repository.CountryRepository;
import com.winesrate.web.repository.StoresRepository;
import com.winesrate.web.search.models.Wine;
import com.winesrate.web.search.repository.WineIndexRepository;

@Service
public class WineIndexServiceImpl implements WineIndexService {

	private static final Logger logger = LoggerFactory.getLogger(WineIndexServiceImpl.class);
	
	@Autowired
	WineIndexRepository wineIndexRepository;
	
	@Autowired
	BrandRepository brandsRepository;
	
	@Autowired
	StoresRepository storesRepository;
	
	@Autowired
	CountryRepository countryRepository;
	
	@Override
	public void indexWine(Wines wineRequest) {
		Wine wine = generateWine(wineRequest);
		try {
			logger.info("Indexing wine to ES");
			logger.info(wine.toString());
			wineIndexRepository.save(wine);
		}
		catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	private Wine generateWine(Wines wineDTO) {
        Wine wine = new Wine();
        wine.setId(wineDTO.getId());
        wine.setAppellation(wineDTO.getAppellation());
        wine.setBottleSize(wineDTO.getBottleSize());
        wine.setColor(wineDTO.getColor());
        wine.setCountry(wineDTO.getCountry());
        wine.setPrice(wineDTO.getPrice());
        wine.setRegion(wineDTO.getRegion());
        wine.setSub_region(wineDTO.getSub_region());
        wine.setVintage(wineDTO.getVintage());
        wine.setTitle(wineDTO.getTitle());
        wine.setURL(wineDTO.getURL());   
        wine.setRating(wineDTO.getRating());        
        
        Brands brand = brandsRepository.findByBrandId(wineDTO.getBrandId());
        wine.setBrand(brand);
        
        Stores store = storesRepository.findByStoreId(wineDTO.getStoreId());
        wine.setStore(store);
        wine.setStoreId(store.getStoreId().toString());
        
        if(wineDTO.getCountry() != null) {
	        Country country = countryRepository.findByCountryName(wineDTO.getCountry());
	        if(country != null) {
	        	String continentName = country.getContinentName();
	        	continentName = CaseFormat.UPPER_UNDERSCORE.to(CaseFormat.UPPER_CAMEL, continentName);
				continentName = continentName.replace(" ", "");
		        wine.setContinent(country.getContinentName());		        
	        }
        }
        
        wine.setSuggest();
        
        return wine;
    }
}