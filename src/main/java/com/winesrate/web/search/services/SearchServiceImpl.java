package com.winesrate.web.search.services;

import static org.elasticsearch.index.query.QueryBuilders.boolQuery;
import static org.elasticsearch.index.query.QueryBuilders.matchAllQuery;
import static org.elasticsearch.index.query.QueryBuilders.matchPhraseQuery;
import static org.elasticsearch.index.query.QueryBuilders.matchQuery;
import static org.elasticsearch.search.aggregations.AggregationBuilders.avg;
import static org.elasticsearch.search.aggregations.AggregationBuilders.terms;
import static org.elasticsearch.search.aggregations.AggregationBuilders.topHits;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

import org.elasticsearch.action.suggest.SuggestResponse;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.MatchQueryBuilder.Type;
import org.elasticsearch.index.query.functionscore.FunctionScoreQueryBuilder;
import org.elasticsearch.index.query.functionscore.ScoreFunctionBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.aggregations.bucket.terms.Terms;
import org.elasticsearch.search.aggregations.metrics.avg.Avg;
import org.elasticsearch.search.aggregations.metrics.tophits.TopHits;
import org.elasticsearch.search.sort.SortBuilders;
import org.elasticsearch.search.sort.SortOrder;
import org.elasticsearch.search.suggest.completion.CompletionSuggestion;
import org.elasticsearch.search.suggest.completion.CompletionSuggestionBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
import org.springframework.data.elasticsearch.core.aggregation.AggregatedPage;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.data.elasticsearch.core.query.SearchQuery;
import org.springframework.stereotype.Service;

import com.google.common.base.CaseFormat;
import com.winesrate.web.dtos.Bucket;
import com.winesrate.web.dtos.LocalStores;
import com.winesrate.web.dtos.RecommendedWines;
import com.winesrate.web.dtos.SearchRequest;
import com.winesrate.web.dtos.SearchResponse;
import com.winesrate.web.dtos.Suggestions;
import com.winesrate.web.dtos.WineAggregation;
import com.winesrate.web.models.Stores;
import com.winesrate.web.search.models.Wine;
import com.winesrate.web.search.repository.LocalStoreRepository;
import com.winesrate.web.search.repository.WineSearchRepository;

@Service
public class SearchServiceImpl implements SearchService {
	
	private static final Logger logger = LoggerFactory.getLogger(SearchServiceImpl.class);
	
	@Autowired 
	ElasticsearchTemplate esTemplate;
	
	@Autowired
	WineSearchRepository wineSearchRepository;
	
	@Autowired
	LocalStoreRepository localStoreRepository;
	
	@Override
	public List<Wine> findWines(String query) {
		List<Wine> wines = wineSearchRepository.findByTitle(query);
		return wines;
	}

	@Override
	public SearchResponse searchWines(SearchRequest request, String sessionId) {		
		
		NativeSearchQueryBuilder searchQueryBuilder = new NativeSearchQueryBuilder();
		NativeSearchQueryBuilder searchAggQueryBuilder = new NativeSearchQueryBuilder();
		NativeSearchQueryBuilder searchRegionAggQueryBuilder = new NativeSearchQueryBuilder();		
		NativeSearchQueryBuilder searchStoreAggQueryBuilder = new NativeSearchQueryBuilder();
		NativeSearchQueryBuilder priceAvgAggQueryBuilder = new NativeSearchQueryBuilder();
		NativeSearchQueryBuilder brandAggQueryBuilder = new NativeSearchQueryBuilder();
		NativeSearchQueryBuilder countryAggQueryBuilder = new NativeSearchQueryBuilder();
		
		
		BoolQueryBuilder qb = boolQuery().must(!request.getQuery().isEmpty() ? matchQuery("title", request.getQuery()).type(Type.PHRASE) : matchAllQuery());
		BoolQueryBuilder aggQb = boolQuery().must(!request.getQuery().isEmpty() ? matchQuery("title", request.getQuery()).type(Type.PHRASE) : matchAllQuery());
		BoolQueryBuilder regionAggQb = boolQuery().must(!request.getQuery().isEmpty() ? matchQuery("title", request.getQuery()).type(Type.PHRASE) : matchAllQuery());
		
		// Random function score
		FunctionScoreQueryBuilder fsqb = new FunctionScoreQueryBuilder(qb);
        fsqb.add(ScoreFunctionBuilders.randomFunction(sessionId));
		
		SortOrder sortOrder = request.getSortOrder().equals("asc") ? SortOrder.ASC : SortOrder.DESC;
		
		if(!request.getContinent().isEmpty()) {
			qb.must(matchQuery("continent", request.getContinent()));
		}
		if(request.getVintage() != null) {
			qb.must(matchQuery("vintage", request.getVintage()));
			regionAggQb.must(matchQuery("vintage", request.getVintage()));
		}
		
		if(request.getSortBy() != null) {						
			searchQueryBuilder.withSort(SortBuilders.fieldSort(request.getSortBy()).order(sortOrder));
		}
		else {						
			searchQueryBuilder.withSort(SortBuilders.fieldSort("price").order(sortOrder));
		}
		
		searchQueryBuilder.withPageable(new PageRequest(request.getPageNumber(), request.getSize()));
		
		searchQueryBuilder.withQuery(fsqb);		
		
		searchAggQueryBuilder
			.withQuery(aggQb)
			.addAggregation(terms("vintage").field("vintage"));
		
		searchRegionAggQueryBuilder
		.withQuery(regionAggQb)
		.addAggregation(terms("continents").field("continent"));
		
		searchStoreAggQueryBuilder
		.withQuery(regionAggQb)
		.addAggregation(terms("stores").field("store.name"));
		
		priceAvgAggQueryBuilder
		.withQuery(qb)
		.addAggregation(avg("price").field("price"));
		
		brandAggQueryBuilder
		.withQuery(qb)
		.addAggregation(terms("brands").field("brands.name"));
		
		countryAggQueryBuilder
		.withQuery(qb)
		.addAggregation(terms("countries").field("country"));
				
        SearchQuery searchQuery = searchQueryBuilder.build();
        SearchQuery aggSearchQuery = searchAggQueryBuilder.build();
        SearchQuery regionAggSearchQuery = searchRegionAggQueryBuilder.build();
        SearchQuery storeAggSearchQuery = searchStoreAggQueryBuilder.build();
        SearchQuery avgPriceAggSearchQuery = priceAvgAggQueryBuilder.build();
        SearchQuery brandAggSearchQuery = brandAggQueryBuilder.build();
        SearchQuery countryAggSearchQuery = countryAggQueryBuilder.build();
        
		AggregatedPage<Wine> resultWines = (AggregatedPage<Wine>) esTemplate.queryForPage(searchQuery,Wine.class);
		AggregatedPage<Wine> resultAggs = (AggregatedPage<Wine>) esTemplate.queryForPage(aggSearchQuery,Wine.class);
		AggregatedPage<Wine> regionAggs = (AggregatedPage<Wine>) esTemplate.queryForPage(regionAggSearchQuery,Wine.class);
		AggregatedPage<Wine> storeResultAggs = (AggregatedPage<Wine>) esTemplate.queryForPage(storeAggSearchQuery, Wine.class);
		AggregatedPage<Wine> avgPriceResultAggs = (AggregatedPage<Wine>) esTemplate.queryForPage(avgPriceAggSearchQuery, Wine.class);
		AggregatedPage<Wine> brandResultAggs = (AggregatedPage<Wine>) esTemplate.queryForPage(brandAggSearchQuery, Wine.class);
		AggregatedPage<Wine> countryResultAggs = (AggregatedPage<Wine>) esTemplate.queryForPage(countryAggSearchQuery, Wine.class);

		Terms agg = regionAggs.getAggregations().get("continents");
		Terms vintageAgg = resultAggs.getAggregations().get("vintage");	
		Terms storeAgg = storeResultAggs.getAggregations().get("stores");
		Avg priceAgg = avgPriceResultAggs.getAggregations().get("price");
		Terms brandAgg = brandResultAggs.getAggregations().get("brands");
		Terms countryAgg = countryResultAggs.getAggregations().get("countries");

		WineAggregation continentAgg = new WineAggregation();
		WineAggregation vintageAggs = new WineAggregation();
		WineAggregation storeAggs = new WineAggregation();
		Double avgPriceAggs = priceAgg.getValue();
		WineAggregation brandAggs = new WineAggregation();
		WineAggregation countryAggs = new WineAggregation();
		
		List<WineAggregation> aggs = new ArrayList<WineAggregation>();
		
		List<Bucket> buckets = new ArrayList<Bucket>();
		List<Bucket> vintageBuckets = new ArrayList<Bucket>();
		List<Bucket> storeBuckets = new ArrayList<Bucket>();
		List<Bucket> brandBuckets = new ArrayList<Bucket>();
		List<Bucket> countryBuckets = new ArrayList<Bucket>();
				
		Bucket b = new Bucket();
		b.setDocCount(0L);
		b.setHref("");
		b.setKey("Worldwide");
		buckets.add(b);				
		
		Bucket vb = new Bucket();
		vb.setDocCount(0L);
		vb.setHref("");
		vb.setKey("All");
		vintageBuckets.add(vb);
		
		HashMap<String, Long> sortedVintage = new HashMap<String, Long>();
		long[] arr = new long[vintageAgg.getBuckets().size()];
		int i = 0;		
		
		for (Terms.Bucket entry : agg.getBuckets()) {
			Bucket bucket = new Bucket();
			String href = CaseFormat.UPPER_UNDERSCORE.to(CaseFormat.UPPER_CAMEL, entry.getKeyAsString());
			href = href.replace(" ", "");
			bucket.setKey(entry.getKeyAsString());
			bucket.setDocCount(entry.getDocCount());
			bucket.setHref(href);
			buckets.add(bucket);
		}
		
		for (Terms.Bucket entry : vintageAgg.getBuckets()) {
			sortedVintage.put(entry.getKeyAsString(), entry.getDocCount());
			arr[i] = Long.parseLong(entry.getKey().toString());
			i++;
		}		
		
		for (Terms.Bucket entry : storeAgg.getBuckets()) {
			Bucket bucket = new Bucket();
			bucket.setKey(entry.getKeyAsString());
			bucket.setDocCount(entry.getDocCount());
			storeBuckets.add(bucket);
		}
		
		for(Terms.Bucket entry : brandAgg.getBuckets()) {
			Bucket bucket = new Bucket();
			bucket.setKey(entry.getKeyAsString());
			bucket.setDocCount(entry.getDocCount());
			brandBuckets.add(bucket);
		}
		
		for(Terms.Bucket entry : countryAgg.getBuckets()) {
			Bucket bucket = new Bucket();
			bucket.setKey(entry.getKeyAsString());
			bucket.setDocCount(entry.getDocCount());
			countryBuckets.add(bucket);
		}
		
		insertionSort(arr);
		for(i = 0; i < arr.length; i++) {
			Bucket bucket = new Bucket();
			String s = new String();
			bucket.setKey(s.valueOf(arr[i]));
			bucket.setDocCount(sortedVintage.get(arr[i]));
			bucket.setHref(s.valueOf(arr[i]));
			vintageBuckets.add(bucket);
		}		
					
		continentAgg.setName("continents");
		continentAgg.setBuckets(buckets);
		
		vintageAggs.setName("vintage");
		vintageAggs.setBuckets(vintageBuckets);
		
		storeAggs.setName("stores");
		storeAggs.setBuckets(storeBuckets);
		
		brandAggs.setName("brands");
		brandAggs.setBuckets(brandBuckets);
		
		countryAggs.setName("countries");
		countryAggs.setBuckets(countryBuckets);
		
		aggs.add(continentAgg);
		aggs.add(vintageAggs);		
		aggs.add(storeAggs);
		aggs.add(brandAggs);
		aggs.add(countryAggs);
		
		SearchResponse response = new SearchResponse();
		
		response.setTotalPages(resultWines.getTotalPages());
		response.setCurrentPageNumber(request.getPageNumber());
		
		response.setWines(resultWines.getContent());
		response.setAggregatations(aggs);
		response.setAvgPrice(avgPriceAggs);	
		
		return response;
	}

	@Override
	public ArrayList<Suggestions> suggestWines(String query) {
		CompletionSuggestionBuilder suggestionBuilder = new CompletionSuggestionBuilder("wineSuggest")
				.text(query).field("suggest");
		SuggestResponse suggestResponse = esTemplate.suggest(suggestionBuilder, Wine.class);
		CompletionSuggestion completionSuggestion = suggestResponse.getSuggest().getSuggestion("wineSuggest");
		List<CompletionSuggestion.Entry.Option> options = completionSuggestion.getEntries().get(0).getOptions();
		
		ArrayList<Suggestions> suggestions = new ArrayList<Suggestions>(); 
		for(int i = 0; i < options.size(); i++) {
			Suggestions s = new Suggestions();
			s.setTitle(options.get(i).getText().string());
			suggestions.add(s);
		}
		return suggestions;
	}
	
	@Override
	public SearchResponse wineGuide(SearchRequest request) {		
		
		NativeSearchQueryBuilder searchQueryBuilder = new NativeSearchQueryBuilder();
		NativeSearchQueryBuilder searchAggQueryBuilder = new NativeSearchQueryBuilder();
		NativeSearchQueryBuilder searchRegionAggQueryBuilder = new NativeSearchQueryBuilder();		
		
		BoolQueryBuilder qb = boolQuery().must(!request.getQuery().isEmpty() ? matchQuery("title", request.getQuery()).type(Type.PHRASE) : matchAllQuery());
		BoolQueryBuilder aggQb = boolQuery().must(!request.getQuery().isEmpty() ? matchQuery("title", request.getQuery()).type(Type.PHRASE) : matchAllQuery());
		BoolQueryBuilder regionAggQb = boolQuery().must(!request.getQuery().isEmpty() ? matchQuery("title", request.getQuery()).type(Type.PHRASE) : matchAllQuery());
		
		SortOrder sortOrder = request.getSortOrder().equals("asc") ? SortOrder.ASC : SortOrder.DESC;
		
		if(!request.getContinent().isEmpty()) {
			qb.must(matchQuery("continent", request.getContinent()));
		}
		if(request.getVintage() != null) {
			qb.must(matchQuery("vintage", request.getVintage()));
			regionAggQb.must(matchQuery("vintage", request.getVintage()));
		}
		
		if(request.getSortBy() != null) {						
			searchQueryBuilder.withSort(SortBuilders.fieldSort(request.getSortBy()).order(sortOrder));
		}
		else {						
			searchQueryBuilder.withSort(SortBuilders.fieldSort("price").order(sortOrder));
		}
		
		searchQueryBuilder.withPageable(new PageRequest(request.getPageNumber(), request.getSize()));
		
		searchQueryBuilder.withQuery(qb);		
		
		searchAggQueryBuilder
			.withQuery(aggQb)
			.addAggregation(terms("vintage").field("vintage"));
		
		searchRegionAggQueryBuilder
		.withQuery(regionAggQb)
		.addAggregation(terms("continents").field("continent"));				
				
        SearchQuery searchQuery = searchQueryBuilder.build();        
        SearchQuery aggSearchQuery = searchAggQueryBuilder.build();        
        SearchQuery regionAggSearchQuery = searchRegionAggQueryBuilder.build();        
        
		AggregatedPage<Wine> resultWines = (AggregatedPage<Wine>) esTemplate.queryForPage(searchQuery,Wine.class);
		AggregatedPage<Wine> resultAggs = (AggregatedPage<Wine>) esTemplate.queryForPage(aggSearchQuery,Wine.class);
		AggregatedPage<Wine> regionAggs = (AggregatedPage<Wine>) esTemplate.queryForPage(regionAggSearchQuery,Wine.class);

		Terms agg = regionAggs.getAggregations().get("continents");
		Terms vintageAgg = resultAggs.getAggregations().get("vintage");		

		WineAggregation continentAgg = new WineAggregation();
		WineAggregation vintageAggs = new WineAggregation();
		List<WineAggregation> aggs = new ArrayList<WineAggregation>();
		
		List<Bucket> buckets = new ArrayList<Bucket>();
		List<Bucket> vintageBuckets = new ArrayList<Bucket>();
				
		Bucket b = new Bucket();
		b.setDocCount(0L);
		b.setHref("");
		b.setKey("Worldwide");
		buckets.add(b);				
		
		Bucket vb = new Bucket();
		vb.setDocCount(0L);
		vb.setHref("");
		vb.setKey("All");
		vintageBuckets.add(vb);
		
		HashMap<String, Long> sortedVintage = new HashMap<String, Long>();
		long[] arr = new long[vintageAgg.getBuckets().size()];
		int i = 0;		
		
		for (Terms.Bucket entry : agg.getBuckets()) {
			Bucket bucket = new Bucket();
			String href = CaseFormat.UPPER_UNDERSCORE.to(CaseFormat.UPPER_CAMEL, entry.getKeyAsString());
			href = href.replace(" ", "");
			bucket.setKey(entry.getKeyAsString());
			bucket.setDocCount(entry.getDocCount());
			bucket.setHref(href);
			buckets.add(bucket);
		}
		
		for (Terms.Bucket entry : vintageAgg.getBuckets()) {
			sortedVintage.put(entry.getKeyAsString(), entry.getDocCount());
			arr[i] = Long.parseLong(entry.getKey().toString());
			i++;
		}		
		
		insertionSort(arr);
		for(i = 0; i < arr.length; i++) {
			Bucket bucket = new Bucket();
			String s = new String();
			bucket.setKey(s.valueOf(arr[i]));
			bucket.setDocCount(sortedVintage.get(arr[i]));
			bucket.setHref(s.valueOf(arr[i]));
			vintageBuckets.add(bucket);
		}
		
		continentAgg.setName("continents");
		continentAgg.setBuckets(buckets);
		
		vintageAggs.setName("vintage");
		vintageAggs.setBuckets(vintageBuckets);
		
		aggs.add(continentAgg);
		aggs.add(vintageAggs);
		
		SearchResponse response = new SearchResponse();
		
		response.setTotalPages(resultWines.getTotalPages());
		response.setCurrentPageNumber(request.getPageNumber());
		
		response.setWines(resultWines.getContent());
		response.setAggregatations(aggs);
		
		
		
		return response;
	}

	@Override
	public List<Wine> singleWine(String query) {
		SearchQuery searchQuery = new NativeSearchQueryBuilder()
			    .withQuery(matchPhraseQuery("title", query))
			    .build();
		Page<Wine> resultWines = (Page<Wine>) esTemplate.queryForPage(searchQuery,Wine.class);
		return resultWines.getContent();
	}
	
	public void insertionSort(long array[]) {  
        int n = array.length;  
        for(int j = 1; j < n; j++) {  
            Long key = array[j];  
            int i = j-1;  
            while ( (i > -1) && ( array [i] > key ) ) {  
                array [i+1] = array [i];  
                i--;  
            }  
            array[i+1] = key;  
        }  
    }

	@Override
	public RecommendedWines recommendedWines() {
		String[] includes = {"price", "url", "store", "appellation", "country", "vintage", "bottleSize"};
		RecommendedWines response = new RecommendedWines();
		int i = 0;
		SearchQuery searchQuery = new NativeSearchQueryBuilder()
	    .withQuery(matchAllQuery())
	    .addAggregation(terms("titles").field("titleNotAnalyzed").subAggregation(topHits("top_wine_hits").setFetchSource(includes, null)).size(8))
	    .build();
		AggregatedPage<Wine> resultWines = (AggregatedPage<Wine>) esTemplate.queryForPage(searchQuery,Wine.class);
		Terms terms = resultWines.getAggregations().get("titles");
		List<Wine> wines = new ArrayList<Wine>();
		for(org.elasticsearch.search.aggregations.bucket.terms.Terms.Bucket b : terms.getBuckets()) {
			Wine w = new Wine();
			w.setTitle(b.getKeyAsString());
			TopHits topHits = terms.getBuckets().get(i).getAggregations().get("top_wine_hits");
			for (SearchHit hit : topHits.getHits().getHits()) {
		        w.setPrice(Float.valueOf(hit.getSource().get("price").toString()));
		        logger.info("Country = " + hit.getSource().get("country").toString());
		        logger.info("Vintage = " + hit.getSource().get("vintage").toString());
		        Stores s = new Stores();
		        HashMap<String, String> st = (HashMap<String, String>) hit.getSource().get("store");
//		        s.setStoreId(Integer.parseInt(st.get("storeId")));
		        s.setStoreURL(st.get("storeURL"));
		        s.setName(st.get("name"));
		        s.setLogoUrl(st.get("logoUrl"));
		        w.setStore(s);
		        w.setAppellation(hit.getSource().get("appellation").toString());
		        w.setVintage(hit.getSource().get("vintage").toString());
		        w.setCountry(hit.getSource().get("country").toString());
		        if(hit.getSource().get("url") != null)
		        	w.setURL(hit.getSource().get("url").toString());
		        if(hit.getSource().get("bottleSize") != null)
		        	w.setBottleSize(hit.getSource().get("bottleSize").toString());
		    }
			wines.add(w);
			i++;			
		}
		
		List<Wine> randomWines = new ArrayList<Wine>();
		List<Integer> keys = new ArrayList<Integer>();
		boolean flag = false;
		i = 0;
		Random rn = new Random();
		while(i <= 7) {
			int key = rn.nextInt(8);
			if(!keys.contains(key)) {
				randomWines.add(wines.get(key));
				keys.add(key);
				i++;
			}
			else {
				continue;
			}
		}			
		response.setWines(randomWines);
		return response;
	}

	@Override
	public List<LocalStores> getLocalStores() {
		Iterable<com.winesrate.web.search.models.LocalStores> iterable = localStoreRepository.findAll(); 
		Iterator<com.winesrate.web.search.models.LocalStores> iterator = iterable.iterator();
		List<LocalStores> response = new ArrayList<LocalStores>();
		while(iterator.hasNext()) {
			com.winesrate.web.search.models.LocalStores store = iterator.next();
			LocalStores ls = new LocalStores();
			ls.setDist(store.getDist());
			ls.setGroup(ls.getGroup());
			ls.setStoreLocation(store.getStoreLocation());
			ls.setManager(store.getManager());
			ls.setStoreId(store.getId());
			ls.setSupv(store.getSupv());
			ls.setTelephone(store.getTelephone());
			ls.setType(store.getType());
			response.add(ls);
		}
		return response;
	}  
}