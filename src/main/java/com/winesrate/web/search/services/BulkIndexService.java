package com.winesrate.web.search.services;

public interface BulkIndexService {
	public String indexScores();
}
