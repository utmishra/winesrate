package com.winesrate.web.search.services;

import com.winesrate.web.models.Wines;

public interface WineIndexService {
	public void indexWine(Wines wine);
}
