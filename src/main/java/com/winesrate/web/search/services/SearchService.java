package com.winesrate.web.search.services;

import java.util.ArrayList;
import java.util.List;

import com.winesrate.web.dtos.LocalStores;
import com.winesrate.web.dtos.RecommendedWines;
import com.winesrate.web.dtos.SearchRequest;
import com.winesrate.web.dtos.SearchResponse;
import com.winesrate.web.dtos.Suggestions;
import com.winesrate.web.search.models.Wine;

public interface SearchService {

	List<Wine> findWines(String query);
	
	SearchResponse searchWines(SearchRequest request, String sessionId);
	
	ArrayList<Suggestions> suggestWines(String query);
	
	List<Wine> singleWine(String query);
	
	RecommendedWines recommendedWines();

	SearchResponse wineGuide(SearchRequest request);
	
	List<LocalStores> getLocalStores();
	
}