package com.winesrate.web.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;

import com.winesrate.web.models.Stores;
import com.winesrate.web.repository.StoresRepository;

public class StoreServiceImpl implements StoreService{

    private static final Logger logger = LoggerFactory.getLogger(StoreServiceImpl.class);

    
    @Autowired
    public StoresRepository storeRepository;
    
    @Override
    public Stores addStore(Stores store) {
        Stores storeResponse = storeRepository.save(store);
        return storeResponse;
    }

    @Override
    public Stores updateStore(Stores store) {
        Stores storeResponse = storeRepository.save(store);
        return storeResponse;
    }

    @Override
    public Boolean deleteStore(Integer storeId) {
        try {
            storeRepository.delete(storeId);
        }
        catch(Exception e) {
            logger.info("Delete failed for storeId = " + storeId);
            return false;
        }
        return true;
    }
    
}
