package com.winesrate.web.services;

import com.winesrate.web.dtos.BlogDTO;
import com.winesrate.web.dtos.BlogsDTO;
import com.winesrate.web.models.Blog;

public interface BlogService {
	public BlogDTO getBlog(Integer blogId);
	public BlogsDTO blogs(Integer pageNumber, Integer size, String sortBy, String sortOrder);
	public BlogsDTO findByCategoryId(Integer categoryId, Integer pageNumber, Integer size, String sortBy, String sortOrder);
	public Blog createBlog(BlogDTO blog);
	public Blog createNews(BlogDTO blog);
	public Blog updateBlog(BlogDTO blog);
	public void deleteBlog(Integer blogId);
}
