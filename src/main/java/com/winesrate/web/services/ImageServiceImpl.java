package com.winesrate.web.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.winesrate.web.dtos.ImageDTO;
import com.winesrate.web.models.Image;
import com.winesrate.web.repository.ImageRepository;

@Service
public class ImageServiceImpl implements ImageService{
	
	@Autowired
	ImageRepository imageRepository;

	@Override
	public Image createImage(ImageDTO imageDto) {
		Image image = new Image();
		image.setImageType(imageDto.getImageType());
		image.setImageUrl(imageDto.getImageURL());
		image = imageRepository.save(image);
		return image;
	}

	@Override
	public Image updateImage(ImageDTO image) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void deleteImage(Integer imageId) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Image getImage(Integer imageId) {
		Image image = imageRepository.findById(imageId);
		return image;
	}
	
}
