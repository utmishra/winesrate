package com.winesrate.web.services;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.winesrate.web.dtos.BlogDTO;
import com.winesrate.web.dtos.BlogsDTO;
import com.winesrate.web.models.Blog;
import com.winesrate.web.models.Category;
import com.winesrate.web.repository.BlogRepository;
import com.winesrate.web.repository.CategoryRepository;

@Service
public class BlogServiceImpl implements BlogService{
	
	private static final Logger logger = LoggerFactory.getLogger(BlogServiceImpl.class);
	
	@Autowired
	BlogRepository blogRepository;
	
	@Autowired
	CategoryRepository categoryRepository;

	@Override
	public Blog createBlog(BlogDTO blogDTO) {
		Blog blog = new Blog();
		blog.setTitle(blogDTO.getTitle());
		blog.setContent(blogDTO.getContent());
		blog.setDescription(blogDTO.getDescription());
		blog.setBlogPictureUrl(blogDTO.getBlogPictureUrl());
		blog.setCategoryId(blogDTO.getCategoryId());
		blog.setAuthor("Admin");
		Blog result = blogRepository.save(blog);
		return result;
	}

	@Override
	public Blog updateBlog(BlogDTO blogDTO) {
		Blog blog = new Blog();
		return null;
	}

	@Override
	public void deleteBlog(Integer blogId) {
		blogRepository.delete(blogId);
	}

	@Override
	public BlogDTO getBlog(Integer blogId) {
		Blog b = blogRepository.getOne(blogId);
		BlogDTO blog = new BlogDTO();
		blog.setBlogId(b.getBlogId());
		blog.setBlogUrl(b.getBlogPictureUrl());
		blog.setContent(b.getContent());		
		blog.setTitle(b.getTitle());
		blog.setDescription(b.getDescription());
		blog.setAuthor(b.getAuthor());
		blog.setCreatedAt(b.getCreatedAt());
		return blog;
	}

	@Override
	public BlogsDTO blogs(Integer pageNumber, Integer size, String sortBy, String sortOrder) {
		Integer limit = size;
		Integer offset = size * pageNumber;
		List<Blog> blogs = blogRepository.getAllBlogs(limit, offset, sortBy, sortOrder);
		List<Category> categories = categoryRepository.findAll();
	    BlogsDTO response = new BlogsDTO();	    
		response.setBlogs(blogs);
		response.setCategories(categories);
		response.setSize(size);
		response.setPageNumber(pageNumber);
		response.setSortBy(sortBy);
		response.setSortOrder(sortOrder);
		return response;
	}

	@Override
	public BlogsDTO findByCategoryId(Integer categoryId, Integer pageNumber, Integer size, String sortBy,
			String sortOrder) {
		Integer limit = size;
		Integer offset = size * pageNumber;
		List<Blog> blogs = blogRepository.findByCategoryId(categoryId, limit, offset, sortBy, sortOrder);
		List<Category> categories = categoryRepository.findAll();
	    BlogsDTO response = new BlogsDTO();	    
		response.setBlogs(blogs);
		response.setCategories(categories);
		response.setSize(size);
		response.setPageNumber(pageNumber);
		response.setSortBy(sortBy);
		response.setSortOrder(sortOrder);
		return response;
	}

	@Override
	public Blog createNews(BlogDTO blogDTO) {
		Blog blog = new Blog();
		blog.setTitle(blogDTO.getTitle());
		blog.setContent(blogDTO.getContent());
		blog.setDescription(blogDTO.getDescription());
		blog.setBlogPictureUrl(blogDTO.getBlogPictureUrl());
		blog.setCategoryId(5);
		blog.setAuthor("Admin");
		Blog result = blogRepository.save(blog);
		return result;
	}
}