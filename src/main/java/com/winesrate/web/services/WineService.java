package com.winesrate.web.services;

import java.util.List;

import com.winesrate.web.dtos.Wine;
import com.winesrate.web.dtos.WineDTO;
import com.winesrate.web.models.Rating;
import com.winesrate.web.models.StoreScore;
import com.winesrate.web.models.Wines;

public interface WineService {
    public Wine getWine(Integer wineId);
    public Wines addWine(WineDTO wineDTO);
    public Wines updateWine(WineDTO wineDTO);
    public Boolean deleteWine(Integer wineId);
    public Rating addRating(Rating rating);
    public List<StoreScore> getStoreScores();
}
