package com.winesrate.web.services;

import com.winesrate.web.dtos.ImageDTO;
import com.winesrate.web.models.Image;

public interface ImageService {
	public Image getImage(Integer imageId);
	public Image createImage(ImageDTO image);
	public Image updateImage(ImageDTO image);
	public void deleteImage(Integer imageId);
}
