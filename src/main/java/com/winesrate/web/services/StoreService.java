package com.winesrate.web.services;

import com.winesrate.web.models.Stores;

public interface StoreService {
    public Stores addStore(Stores store);
    public Stores updateStore(Stores store);
    public Boolean deleteStore(Integer storeId);
}
