package com.winesrate.web.services;

import java.io.File;
import java.io.IOException;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.MappingIterator;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;
import com.winesrate.web.dtos.WineDTO;
import com.winesrate.web.models.Stores;
import com.winesrate.web.repository.StoresRepository;

@Service
public class AdminServiceImpl implements AdminService {
	
	private static final Logger logger = LoggerFactory.getLogger(AdminServiceImpl.class);
	
	@Autowired
	WineService wineService;
	
	@Autowired
    StoresRepository storeRepository;

	@Override
	public void indexCSV(File file, String storeName, String storeURL, String logoURL) {
		WineDTO w = new WineDTO();
		
        CsvMapper mapper = new CsvMapper();
        CsvSchema schema = CsvSchema.emptySchema().withHeader(); // use first row as header; otherwise defaults are fine
        
        MappingIterator<Map<String, String>> it = null;
		try {
			it = mapper.readerFor(Map.class).with(schema).readValues(file);
		} catch (IOException e) {
			logger.error("CSV File parse error");
			e.printStackTrace();
		}
		int i = 0;
		
		Stores store = storeRepository.findByName(storeName);
        if(store == null) {
        	store = new Stores();
        	store.setName(storeName);
        	store.setStoreURL(storeURL);
        	store.setLogoUrl(logoURL);
        	store = storeRepository.save(store);
        }
		
        while (it.hasNext()) {
          Map<String,String> row = it.next();
          w.setTitle(row.get("Wine Title"));
          w.setVintage(row.get("Vintage"));
          w.setCountry(row.get("Country"));
          w.setRegion(row.get("Region"));
          w.setSub_region(row.get("Sub region"));
          w.setAppellation(row.get("Appellation"));
          w.setColor(row.get("Color"));
          w.setBottleSize(row.get("Bottle Size"));
          w.setPrice(Float.parseFloat(row.get("Price")));
          w.setURL(row.get("URL"));
          w.setStoreName(storeName);          
          try {
        	  wineService.addWine(w);
          }
          catch(Exception e) {
        	  logger.error("Single row save Wine failed. Row number = " + i);
        	  e.printStackTrace();
          }
          i++;
        }
	}
}