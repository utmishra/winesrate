package com.winesrate.web.services;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.winesrate.web.dtos.Wine;
import com.winesrate.web.dtos.WineDTO;
import com.winesrate.web.dtos.WineResponse;
import com.winesrate.web.models.Brands;
import com.winesrate.web.models.Rating;
import com.winesrate.web.models.StoreScore;
import com.winesrate.web.models.Stores;
import com.winesrate.web.models.Wines;
import com.winesrate.web.repository.BrandRepository;
import com.winesrate.web.repository.RatingRepository;
import com.winesrate.web.repository.StoresRepository;
import com.winesrate.web.repository.WineRepository;
import com.winesrate.web.search.services.WineIndexService;

@Service
public class WineServiceImpl implements WineService {
    
    private static final Logger logger = LoggerFactory.getLogger(WineServiceImpl.class);
    
    @Autowired
    WineRepository wineRepository;
    
    @Autowired
    BrandRepository brandsRepository;
    
    @Autowired
    StoresRepository storeRepository;
    
    @Autowired
    RatingRepository ratingRepository;
    
    @Autowired
    WineIndexService wineIndexService;
    
    @Override
    public Wine getWine(Integer wineId) {
        Wines wine = wineRepository.findById(wineId);
        Wine w = new Wine();
        if(wine != null) {
	        WineResponse wineResp = new WineResponse();
	        wineResp.setAppellation(wine.getAppellation());
	        wineResp.setBottleSize(wine.getBottleSize());
	        wineResp.setColor(wine.getColor());
	        wineResp.setCountry(wine.getCountry());
	        wineResp.setId(wine.getId());
	        wineResp.setPrice(wine.getPrice());
	        wineResp.setRegion(wine.getRegion());
	        wineResp.setTitle(wine.getTitle());
	        wineResp.setSub_region(wine.getSub_region());
	        wineResp.setVintage(wine.getVintage());
	        wineResp.setURL(wine.getURL());
	        
	        Stores store = storeRepository.findByStoreId(wine.getStoreId());
	        wineResp.setStore(store);
	        
	        Brands brand = brandsRepository.findByBrandId(wine.getBrandId());
	        wineResp.setBrand(brand);
	        
	        w.setWine(wineResp);	        
	        return w;
        }
        else
        	return null;
    }

    @Override
    public Wines addWine(WineDTO wineDTO) {
        Wines wine = createWine(wineDTO);
        Wines responseWine = null;
        try {
        	responseWine = wineRepository.save(wine);
	    }
	    catch(Exception e) {
	    	logger.info("SAVE_WINE_TO_SQL_ERROR");
	    	e.printStackTrace();
	    }
	    
	    try {
	    	wineIndexService.indexWine(wine);
	    }
	    catch(Exception e) {
	    	logger.info("INDEX_WINE_TO_ES_ERROR");
	    	e.printStackTrace();
	    }
        
        return responseWine;
    }

    @Override
    public Wines updateWine(WineDTO wineDTO) {
        Wines wine = createWine(wineDTO);        
        Wines responseWine = wineRepository.save(wine);
        
        try {
	    	wineIndexService.indexWine(wine);
	    }
	    catch(Exception e) {
	    	logger.info("UPDATE_WINE_TO_ES_ERROR");
	    	e.printStackTrace();
	    }
        
        return responseWine;
    }

    @Override
    public Boolean deleteWine(Integer wineId) {
        try {
            wineRepository.delete(wineId);
            return true;
        }
        catch(Exception e) {
            logger.info("WINE_DELETE_ERROR");
            e.printStackTrace();
            return false;
        }
    }
    
    private Wines createWine(WineDTO wineDTO) {
        Wines wine = new Wines();
        wine.setAppellation(wineDTO.getAppellation());
        wine.setBottleSize(wineDTO.getBottleSize());
        wine.setColor(wineDTO.getColor());
        wine.setCountry(wineDTO.getCountry());
        wine.setPrice(wineDTO.getPrice());
        wine.setRegion(wineDTO.getRegion());
        wine.setSub_region(wineDTO.getSub_region());
        wine.setVintage(wineDTO.getVintage());
        wine.setTitle(wineDTO.getTitle());
        wine.setURL(wineDTO.getURL());
        
//		Ignoring Brands for now        
//        Brands brand = brandsRepository.findByName(wineDTO.getBrandName());
//        if(brand == null) {
//        	brand = new Brands();
//        	if(wineDTO.getBrandName() != null) {
//        		brand.setName(wineDTO.getBrandName());
//        		brand = brandsRepository.save(brand);
//        	}
//        	else {
//        		brand.setBrandId(0);
//        	}
//        }
//        Integer brandId = brand.getBrandId() != null ? brand.getBrandId() : 0;
//        wine.setBrandId(brandId);
        
        Stores store = storeRepository.findByName(wineDTO.getStoreName());
        if(store == null) {
        	store = new Stores();
        	if(wineDTO.getStoreName() != null) {
	        	store.setName(wineDTO.getStoreName());
	        	store = storeRepository.save(store);
        	}
        	else {
        		store.setStoreId(0);
        	}
        }
        Integer storeId = store.getStoreId() != null ? store.getStoreId() : 0;
        wine.setStoreId(storeId);
        return wine;
    }

	@Override
	public Rating addRating(Rating rating) {
		Rating savedRating = ratingRepository.save(rating);
		Wines wine = wineRepository.findById(rating.getWineId());
		Float updatedRating = ratingRepository.getAvgRatingForWineId(rating.getWineId());
		wine.setRating(updatedRating);
		logger.info("Rating = " + wine.getRating());
		wineRepository.save(wine);
        
        try {
	    	wineIndexService.indexWine(wine);
	    }
	    catch(Exception e) {
	    	logger.info("UPDATE_WINE_TO_ES_ERROR");
	    	e.printStackTrace();
	    }
		return savedRating;
	}

	@Override
	public List<StoreScore> getStoreScores() {
		List<StoreScore> storeScores = new ArrayList<StoreScore>();
		List<Object[]> result = wineRepository.getAllStoreCounts();
		logger.info("Size = " + result.size());
		for(int i = 0; i < result.size(); i++) {
			logger.info("WineId = " + result.get(i)[0]);;
			logger.info("StoreScore = " + result.get(i)[1]);;
			Long wineId = Long.parseLong(result.get(i)[0].toString());
			Long storeCount = Long.parseLong(result.get(i)[1].toString());
			storeScores.add(new StoreScore(wineId, storeCount));
		}
		return storeScores;
	}   
}