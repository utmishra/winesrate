package com.winesrate.web.dtos;

import java.util.List;

import com.winesrate.web.search.models.Wine;

public class RecommendedWines {
	List<Wine> wines;

	public List<Wine> getWines() {
		return wines;
	}

	public void setWines(List<Wine> wines) {
		this.wines = wines;
	}

	@Override
	public String toString() {
		return "RecommendedWines [wines=" + wines + "]";
	}	
}
