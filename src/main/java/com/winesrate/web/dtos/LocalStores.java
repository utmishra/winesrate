package com.winesrate.web.dtos;

import java.io.Serializable;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;

public class LocalStores implements Serializable {
	
	private static final long serialVersionUID = -2603026103837245569L;
	private Long storeId;
	@NotNull
	@NotBlank
	private String supv;
	private String dist;
	private String type;
	private String group;
	@NotNull
	@NotBlank
	private String manager;
	@NotNull
	@NotBlank
	private String storeLocation;
	@NotNull
	@NotBlank
	private String telephone;
	@NotNull
	@NotBlank
	private String website;
	@NotNull
	@NotBlank
	private String email;
	public Long getStoreId() {
		return storeId;
	}
	public void setStoreId(Long storeId) {
		this.storeId = storeId;
	}
	public String getSupv() {
		return supv;
	}
	public void setSupv(String supv) {
		this.supv = supv;
	}
	public String getDist() {
		return dist;
	}
	public void setDist(String dist) {
		this.dist = dist;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getGroup() {
		return group;
	}
	public void setGroup(String group) {
		this.group = group;
	}
	public String getManager() {
		return manager;
	}
	public void setManager(String manager) {
		this.manager = manager;
	}	
	public String getTelephone() {
		return telephone;
	}
	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}
	public String getStoreLocation() {
		return storeLocation;
	}
	public void setStoreLocation(String storeLocation) {
		this.storeLocation = storeLocation;
	}
	public String getWebsite() {
		return website;
	}
	public void setWebsite(String website) {
		this.website = website;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}			
}