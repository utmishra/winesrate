package com.winesrate.web.dtos;

public class Bucket {
	public String key;
	public String href;
	public Long docCount;
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}	
	public String getHref() {
		return href;
	}
	public void setHref(String href) {
		this.href = href;
	}
	public Long getDocCount() {
		return docCount;
	}
	public void setDocCount(Long docCount) {
		this.docCount = docCount;
	}	
}
