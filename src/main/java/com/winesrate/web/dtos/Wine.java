package com.winesrate.web.dtos;

public class Wine {
        
    public WineResponse wine;   

    public WineResponse getWine() {
        return wine;
    }

    public void setWine(WineResponse wine) {
        this.wine = wine;
    }    
}
