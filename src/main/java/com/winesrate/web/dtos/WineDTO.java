package com.winesrate.web.dtos;

public class WineDTO {
    
    public Integer id;
    public String title;
    public String vintage;
    public String country;
    public String region;
    public String sub_region;
    public String appellation;
    public String color;
    public String bottleSize;
    public Float price;
    public String storeName;
    public String brandName;
    public String URL;
    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }
    public String getTitle() {
        return title;
    }
    public void setTitle(String title) {
        this.title = title;
    }
    public String getVintage() {
        return vintage;
    }
    public void setVintage(String vintage) {
        this.vintage = vintage;
    }
    public String getCountry() {
        return country;
    }
    public void setCountry(String country) {
        this.country = country;
    }
    public String getRegion() {
        return region;
    }
    public void setRegion(String region) {
        this.region = region;
    }
    public String getSub_region() {
        return sub_region;
    }
    public void setSub_region(String sub_region) {
        this.sub_region = sub_region;
    }
    public String getAppellation() {
        return appellation;
    }
    public void setAppellation(String appellation) {
        this.appellation = appellation;
    }
    public String getColor() {
        return color;
    }
    public void setColor(String color) {
        this.color = color;
    }
    public String getBottleSize() {
        return bottleSize;
    }
    public void setBottleSize(String bottleSize) {
        this.bottleSize = bottleSize;
    }
    public Float getPrice() {
        return price;
    }
    public void setPrice(Float price) {
        this.price = price;
    }
    public String getStoreName() {
        return storeName;
    }
    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }
    public String getBrandName() {
        return brandName;
    }
    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }
    public String getURL() {
		return URL;
	}
	public void setURL(String uRL) {
		URL = uRL;
	}
	@Override
	public String toString() {
		return "WineDTO [id=" + id + ", title=" + title + ", vintage=" + vintage + ", country=" + country + ", region="
				+ region + ", sub_region=" + sub_region + ", appellation=" + appellation + ", color=" + color
				+ ", bottleSize=" + bottleSize + ", price=" + price + ", storeName=" + storeName + ", brandName="
				+ brandName + ", URL=" + URL + "]";
	}	
}
