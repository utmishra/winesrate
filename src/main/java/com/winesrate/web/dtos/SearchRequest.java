package com.winesrate.web.dtos;

import java.io.Serializable;

public class SearchRequest implements Serializable {
	
	private static final long serialVersionUID = -732568504386925466L;

	private String query;
	
	private String continent;
	
	private Integer storeId;
	
	private Integer brandId;
	
	private Integer vintage;
	
	private String sortBy;
	
	private String sortOrder;
	
	private Integer pageNumber;
	
	private Integer size;

	public String getQuery() {
		return query;
	}

	public void setQuery(String query) {
		this.query = query;
	}

	public Integer getStoreId() {
		return storeId;
	}

	public void setStoreId(Integer storeId) {
		this.storeId = storeId;
	}

	public Integer getBrandId() {
		return brandId;
	}

	public void setBrandId(Integer brandId) {
		this.brandId = brandId;
	}
	
	public String getContinent() {
		return continent;
	}

	public void setContinent(String continent) {
		this.continent = continent;
	}
	
	public Integer getVintage() {
		return vintage;
	}

	public void setVintage(Integer vintage) {
		this.vintage = vintage;
	}
	
	public String getSortBy() {
		return sortBy;
	}

	public void setSortBy(String sortBy) {
		this.sortBy = sortBy;
	}

	public Integer getPageNumber() {
		return pageNumber;
	}

	public void setPageNumber(Integer pageNumber) {
		this.pageNumber = pageNumber;
	}

	public Integer getSize() {
		return size;
	}

	public void setSize(Integer size) {
		this.size = size;
	}
	
	public String getSortOrder() {
		return sortOrder;
	}

	public void setSortOrder(String sortOrder) {
		this.sortOrder = sortOrder;
	}

	@Override
	public String toString() {
		return "SearchRequest [query=" + query + ", continent=" + continent + ", storeId=" + storeId + ", brandId="
				+ brandId + ", vintage=" + vintage + ", sortBy=" + sortBy + ", sortOrder=" + sortOrder + ", pageNumber="
				+ pageNumber + ", size=" + size + "]";
	}	
}
