package com.winesrate.web.dtos;

import java.util.List;

import com.winesrate.web.models.Blog;
import com.winesrate.web.models.Category;

public class BlogsDTO {
	public List<Blog> blogs;
	public List<Category> categories;
	public Integer pageNumber;
	public Integer size;
	public String sortBy;
	public String sortOrder;
	public List<Blog> getBlogs() {
		return blogs;
	}
	public void setBlogs(List<Blog> blogs) {
		this.blogs = blogs;
	}
	public Integer getPageNumber() {
		return pageNumber;
	}
	public void setPageNumber(Integer pageNumber) {
		this.pageNumber = pageNumber;
	}
	public Integer getSize() {
		return size;
	}
	public void setSize(Integer size) {
		this.size = size;
	}
	public String getSortBy() {
		return sortBy;
	}
	public void setSortBy(String sortBy) {
		this.sortBy = sortBy;
	}
	public String getSortOrder() {
		return sortOrder;
	}
	public void setSortOrder(String sortOrder) {
		this.sortOrder = sortOrder;
	}
	public List<Category> getCategories() {
		return categories;
	}
	public void setCategories(List<Category> categories) {
		this.categories = categories;
	}
	@Override
	public String toString() {
		return "BlogsDTO [blogs=" + blogs + ", categories=" + categories + ", pageNumber=" + pageNumber + ", size="
				+ size + ", sortBy=" + sortBy + ", sortOrder=" + sortOrder + "]";
	}	
}
