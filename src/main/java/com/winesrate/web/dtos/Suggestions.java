package com.winesrate.web.dtos;

public class Suggestions {
	private String title;
	private String appellation;
	private String region;
	private String country;
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getAppellation() {
		return appellation;
	}
	public void setAppellation(String appellation) {
		this.appellation = appellation;
	}
	public String getRegion() {
		return region;
	}
	public void setRegion(String region) {
		this.region = region;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	@Override
	public String toString() {
		return "Suggestions [title=" + title + ", appellation=" + appellation + ", region=" + region + ", country="
				+ country + "]";
	}		
}
