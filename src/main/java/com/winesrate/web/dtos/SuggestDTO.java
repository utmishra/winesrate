package com.winesrate.web.dtos;

import java.util.ArrayList;

public class SuggestDTO {	
	private ArrayList<Suggestions> suggestions;

	public ArrayList<Suggestions> getSuggestions() {
		return suggestions;
	}

	public void setSuggestions(ArrayList<Suggestions> suggestions) {
		this.suggestions = suggestions;
	}

	@Override
	public String toString() {
		return "SuggestDTO [suggestions=" + suggestions + "]";
	}
}
