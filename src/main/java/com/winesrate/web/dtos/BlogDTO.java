package com.winesrate.web.dtos;

import java.io.Serializable;
import java.util.Date;

public class BlogDTO implements Serializable {        
	private static final long serialVersionUID = -8190210649303652360L;
	public Integer blogId;
    public String title;
    public String description;
    public String content;
    public String blogPictureUrl;
    public String author;
    public Date createdAt;
    public Integer categoryId;
    
	public Integer getBlogId() {
		return blogId;
	}
	public void setBlogId(Integer blogId) {
		this.blogId = blogId;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}					
	
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public void setBlogPictureUrl(String blogPictureUrl) {
		this.blogPictureUrl = blogPictureUrl;
	}
	public String getBlogPictureUrl() {
		return blogPictureUrl;
	}
	public void setBlogUrl(String blogUrl) {
		this.blogPictureUrl = blogUrl;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	public Date getCreatedAt() {
		return createdAt;
	}
	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}
	public Integer getCategoryId() {
		return categoryId;
	}
	public void setCategoryId(Integer categoryId) {
		this.categoryId = categoryId;
	}
	@Override
	public String toString() {
		return "BlogDTO [blogId=" + blogId + ", title=" + title + ", description=" + description + ", content="
				+ content + ", blogPictureUrl=" + blogPictureUrl + ", author=" + author + ", createdAt=" + createdAt
				+ ", categoryId=" + categoryId + "]";
	}	
}   