package com.winesrate.web.dtos;

import com.winesrate.web.models.Brands;
import com.winesrate.web.models.Stores;

public class WineResponse {
    
    private Integer id;
    private String title;
    private String vintage;
    private String country;
    private String region;
    private String sub_region;
    private String appellation;
    private String color;
    private String bottleSize;
    private Float price;
    private Stores store;
    private Brands brand;
    private String URL;
    
    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }
    public String getTitle() {
        return title;
    }
    public void setTitle(String title) {
        this.title = title;
    }
    public String getVintage() {
        return vintage;
    }
    public void setVintage(String vintage) {
        this.vintage = vintage;
    }
    public String getCountry() {
        return country;
    }
    public void setCountry(String country) {
        this.country = country;
    }
    public String getRegion() {
        return region;
    }
    public void setRegion(String region) {
        this.region = region;
    }
    public String getSub_region() {
        return sub_region;
    }
    public void setSub_region(String sub_region) {
        this.sub_region = sub_region;
    }
    public String getAppellation() {
        return appellation;
    }
    public void setAppellation(String appellation) {
        this.appellation = appellation;
    }
    public String getColor() {
        return color;
    }
    public void setColor(String color) {
        this.color = color;
    }
    public String getBottleSize() {
        return bottleSize;
    }
    public void setBottleSize(String bottleSize) {
        this.bottleSize = bottleSize;
    }
    public Float getPrice() {
        return price;
    }
    public void setPrice(Float price) {
        this.price = price;
    }
    public Stores getStore() {
        return store;
    }
    public void setStore(Stores store) {
        this.store = store;
    }
    public Brands getBrand() {
        return brand;
    }
    public void setBrand(Brands brand) {
        this.brand = brand;
    }
    public String getURL() {
		return URL;
	}
	public void setURL(String uRL) {
		URL = uRL;
	}
	@Override
	public String toString() {
		return "WineResponse [id=" + id + ", title=" + title + ", vintage=" + vintage + ", country=" + country
				+ ", region=" + region + ", sub_region=" + sub_region + ", appellation=" + appellation + ", color="
				+ color + ", bottleSize=" + bottleSize + ", price=" + price + ", store=" + store + ", brand=" + brand
				+ ", URL=" + URL + "]";
	}
}
