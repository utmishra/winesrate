package com.winesrate.web.dtos;

import java.util.List;

public class WineAggregation {
	
	private String name;
	private List<Bucket> buckets;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public List<Bucket> getBuckets() {
		return buckets;
	}
	public void setBuckets(List<Bucket> buckets) {
		this.buckets = buckets;
	}	
}
