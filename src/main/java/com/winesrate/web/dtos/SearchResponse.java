package com.winesrate.web.dtos;

import java.util.List;

import com.winesrate.web.search.models.Wine;

public class SearchResponse {
	
	private String query;
	private List<Wine> wines;
	private List<WineAggregation> aggregatations;
	private Double avgPrice;
	private Integer totalPages;
	private Integer currentPageNumber;
	private Integer firstPageNav;
	private Integer lastPageNav;
	private String sortBy;
	private String sortOrder;
	private Integer vintage;
	private String continent;
	private List<Wine> recommended;
	public List<Wine> getWines() {
		return wines;
	}
	public void setWines(List<Wine> wines) {
		this.wines = wines;
	}
	public List<WineAggregation> getAggregatations() {
		return aggregatations;
	}
	public void setAggregatations(List<WineAggregation> aggregatations) {
		this.aggregatations = aggregatations;
	}
	public Integer getTotalPages() {
		return totalPages;
	}
	public void setTotalPages(Integer totalPages) {
		this.totalPages = totalPages;
	}
	public Integer getCurrentPageNumber() {
		return currentPageNumber;
	}
	public void setCurrentPageNumber(Integer currentPageNumber) {
		this.currentPageNumber = currentPageNumber;
	}	
	public String getQuery() {
		return query;
	}
	public void setQuery(String query) {
		this.query = query;
	}
	public Integer getLastPageNav() {
		return lastPageNav;
	}
	public void setLastPageNav(Integer lastPageNav) {
		this.lastPageNav = lastPageNav;
	}
	public Integer getFirstPageNav() {
		return firstPageNav;
	}
	public void setFirstPageNav(Integer firstPageNav) {
		this.firstPageNav = firstPageNav;
	}
	public String getSortBy() {
		return sortBy;
	}
	public void setSortBy(String sortBy) {
		this.sortBy = sortBy;
	}
	public String getSortOrder() {
		return sortOrder;
	}
	public void setSortOrder(String sortOrder) {
		this.sortOrder = sortOrder;
	}
	public Integer getVintage() {
		return vintage;
	}
	public void setVintage(Integer vintage) {
		this.vintage = vintage;
	}
	public String getContinent() {
		return continent;
	}
	public void setContinent(String continent) {
		this.continent = continent;
	}
	public Double getAvgPrice() {
		return avgPrice;
	}
	public void setAvgPrice(Double avgPrice) {
		this.avgPrice = avgPrice;
	}
	public List<Wine> getRecommended() {
		return recommended;
	}
	public void setRecommended(List<Wine> recommended) {
		this.recommended = recommended;
	}	
	@Override
	public String toString() {
		return "SearchResponse [query=" + query + ", wines=" + wines + ", aggregatations=" + aggregatations
				+ ", totalPages=" + totalPages + ", currentPageNumber=" + currentPageNumber + ", firstPageNav="
				+ firstPageNav + ", lastPageNav=" + lastPageNav + ", sortBy=" + sortBy + ", sortOrder=" + sortOrder
				+ "]";
	}
}
