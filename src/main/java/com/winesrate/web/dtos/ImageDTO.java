package com.winesrate.web.dtos;

public class ImageDTO {
	public String imageId;
	public String imageType;
	public String imageURL;
	
	public String getImageId() {
		return imageId; 
	}

	public void setImageId(String imageId) {
		this.imageId = imageId;
	}

	public String getImageType() {
		return imageType;
	}

	public void setImageType(String imageType) {
		this.imageType = imageType;
	}

	public String getImageURL() {
		return imageURL;
	}

	public void setImageURL(String imageURL) {
		this.imageURL = imageURL;
	}

	@Override
	public String toString() {
		return "ImageDTO [imageId=" + imageId + ", imageType=" + imageType + ", imageURL=" + imageURL + "]";
	}
}
