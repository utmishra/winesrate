package com.winesrate.web.models;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.winesrate.web.services.BlogServiceImpl;

@Entity
@Table(name = "blogs")
public class Blog {
	
	private static final Logger logger = LoggerFactory.getLogger(Blog.class);
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer blogId;
    @Column(nullable = false)
    private String title;
    @Column(nullable = false)
    private String description;
    @Column(name = "timestamp", nullable = false, updatable = false, insertable = false, columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
    private Date createdAt;
//    @Temporal(TemporalType.TIMESTAMP)
//    @Column(name = "updatedAt", columnDefinition="DATETIME DEFAULT CURRENT_DATETIME ON UPDATE CURRENT_DATETIME")
    private Date updatedAt;
    @Lob
    @Column(nullable = false, length = 100000, columnDefinition = "LONGBLOB")
    private String content;
    @Column(nullable = false)
    private String blogPictureUrl;
    @Column(columnDefinition="VARCHAR(30) default 'Admin'")
    private String author;
    private Integer categoryId;
	public Integer getBlogId() {
		return blogId;
	}
	public void setBlogId(Integer blogId) {
		this.blogId = blogId;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}	
	public String getBlogPictureUrl() {
		return blogPictureUrl;
	}
	public void setBlogPictureUrl(String blogPictureUrl) {
		this.blogPictureUrl = blogPictureUrl;
	}
	public Date getCreatedAt() {
		return createdAt;
	}
	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}
	public Date getUpdatedAt() {
		return updatedAt;
	}
	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}			
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	public Integer getCategoryId() {
		return categoryId;
	}
	public void setCategoryId(Integer categoryId) {
		this.categoryId = categoryId;
	}	
	@Override
	public String toString() {
		return "Blog [blogId=" + blogId + ", title=" + title + ", description=" + description + ", createdAt="
				+ createdAt + ", updatedAt=" + updatedAt + ", content=" + content + ", blogPictureUrl=" + blogPictureUrl
				+ ", author=" + author + "]";
	}	
}   