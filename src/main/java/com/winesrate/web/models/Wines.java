package com.winesrate.web.models;

import java.sql.Date;
import java.util.ArrayList;
import java.util.HashMap;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "wines")
public class Wines {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Integer id;
    public String title;
    public String vintage;
    public String country;
    public String continent;
    public String region;
    public String sub_region;
    public String appellation;
    public String color;
    public String bottleSize;
    public Float price;
    public Integer storeId;
    public Integer brandId;
    @Column(name = "createdAt", columnDefinition="DATETIME")
    private Date createdAt;
    @Column(name = "updatedAt", columnDefinition="DATETIME")
    private Date updatedAt;  
    private String URL;  
    private String imageURL;
    private Float rating;
    
    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }
    public String getTitle() {
        return title;
    }
    public void setTitle(String title) {
        this.title = title;
    }
    public String getVintage() {
        return vintage;
    }
    public void setVintage(String vintage) {
        this.vintage = vintage;
    }
    public String getCountry() {
        return country;
    }
    public void setCountry(String country) {
        this.country = country;
    }    
    public String getContinent() {
		return continent;
	}
	public void setContinent(String continent) {
		this.continent = continent;
	}
	public String getRegion() {
        return region;
    }
    public void setRegion(String region) {
        this.region = region;
    }
    public String getSub_region() {
        return sub_region;
    }
    public void setSub_region(String sub_region) {
        this.sub_region = sub_region;
    }
    public String getAppellation() {
        return appellation;
    }
    public void setAppellation(String appellation) {
        this.appellation = appellation;
    }
    public String getColor() {
        return color;
    }
    public void setColor(String color) {
        this.color = color;
    }
    public String getBottleSize() {
        return bottleSize;
    }
    public void setBottleSize(String bottleSize) {
        this.bottleSize = bottleSize;
    }
    public Float getPrice() {
        return price;
    }
    public void setPrice(Float price) {
        this.price = price;
    }
    public Integer getStoreId() {
        return storeId;
    }
    public void setStoreId(Integer storeId) {
        this.storeId = storeId;
    }
    public Integer getBrandId() {
        return brandId;
    }
    public void setBrandId(Integer brandId) {
        this.brandId = brandId;
    }    
	public Date getCreatedAt() {
		return createdAt;
	}
	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}
	public Date getUpdatedAt() {
		return updatedAt;
	}
	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}
	public String getURL() {
		return URL;
	}
	public void setURL(String uRL) {
		URL = uRL;
	}
	public String getImageURL() {
		return imageURL;
	}
	public void setImageURL(String imageURL) {
		this.imageURL = imageURL;
	}
	public Float getRating() {
		return rating;
	}
	public void setRating(Float rating) {
		this.rating = rating;
	}
	@Override
	public String toString() {
		return "Wines [id=" + id + ", title=" + title + ", vintage=" + vintage + ", country=" + country + ", continent="
				+ continent + ", region=" + region + ", sub_region=" + sub_region + ", appellation=" + appellation
				+ ", color=" + color + ", bottleSize=" + bottleSize + ", price=" + price + ", storeId=" + storeId
				+ ", brandId=" + brandId + ", createdAt=" + createdAt + ", updatedAt=" + updatedAt + ", URL=" + URL
				+ ", imageURL=" + imageURL + "]";
	}		
}
