package com.winesrate.web.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "stores")
public class Stores {
    @Id    
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Integer storeId;
    public String name;
    @Column(name = "createdAt", columnDefinition="DATETIME")
    private String createdAt;
    @Column(name = "updatedAt", columnDefinition="DATETIME")
    private String updatedAt;
    private String storeURL;
    private String logoUrl;
    public Integer getStoreId() {
        return storeId;
    }
    public void setStoreId(Integer storeId) {
        this.storeId = storeId;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
	public String getCreatedAt() {
		return createdAt;
	}
	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}
	public String getUpdatedAt() {
		return updatedAt;
	}
	public void setUpdatedAt(String updatedAt) {
		this.updatedAt = updatedAt;
	}
	public String getStoreURL() {
		return storeURL;
	}
	public void setStoreURL(String storeURL) {
		this.storeURL = storeURL;
	}
	public String getLogoUrl() {
		return logoUrl;
	}
	public void setLogoUrl(String logoUrl) {
		this.logoUrl = logoUrl;
	}
	@Override
	public String toString() {
		return "Stores [storeId=" + storeId + ", name=" + name + ", createdAt=" + createdAt + ", updatedAt=" + updatedAt
				+ ", storeURL=" + storeURL + "]";
	}	
}