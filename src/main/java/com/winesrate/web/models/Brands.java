package com.winesrate.web.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "brands")
public class Brands {
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer brandId;
    private String name;
    @Column(name = "createdAt", columnDefinition="DATETIME")
    private String createdAt;
    @Column(name = "updatedAt", columnDefinition="DATETIME")
    private String updatedAt;
    public Integer getBrandId() {
        return brandId;
    }
    public void setBrandId(Integer brandId) {
        this.brandId = brandId;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
	public String getCreatedAt() {
		return createdAt;
	}
	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}
	public String getUpdatedAt() {
		return updatedAt;
	}
	public void setUpdatedAt(String updatedAt) {
		this.updatedAt = updatedAt;
	}
	@Override
	public String toString() {
		return "Brands [brandId=" + brandId + ", name=" + name + ", createdAt=" + createdAt + ", updatedAt=" + updatedAt
				+ "]";
	}
}
