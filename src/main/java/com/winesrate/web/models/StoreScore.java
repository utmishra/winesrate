package com.winesrate.web.models;

public class StoreScore {
	public Long storeId;
	public Long storeCount;
	
	public StoreScore(Long storeId, Long storeCount) {
		this.storeId = storeId;
		this.storeCount = storeCount;
	}
}
