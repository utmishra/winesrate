package com.winesrate.web.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "ratings")
public class Rating {
	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
	private Integer ratingId;
	private Integer wineId;
	private Float rating;
	public Integer getRatingId() {
		return ratingId;
	}
	public void setRatingId(Integer ratingId) {
		this.ratingId = ratingId;
	}
	public Integer getWineId() {
		return wineId;
	}
	public void setWineId(Integer wineId) {
		this.wineId = wineId;
	}
	public Float getRating() {
		return rating;
	}
	public void setRating(Float rating) {
		this.rating = rating;
	}
	@Override
	public String toString() {
		return "Rating [ratingId=" + ratingId + ", wineId=" + wineId + ", rating=" + rating + "]";
	}	
}
