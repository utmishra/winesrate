package com.winesrate.web.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.winesrate.web.models.StoreScore;
import com.winesrate.web.models.Wines;

@Repository
public interface WineRepository extends JpaRepository<Wines, Integer> {
    public Wines findById(Integer id);
    @Query(value = "SELECT store_id, COUNT(*) count FROM wines GROUP BY store_id", nativeQuery = true)
    public List<Object[]> getAllStoreCounts();
}