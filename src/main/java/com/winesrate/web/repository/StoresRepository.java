package com.winesrate.web.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.winesrate.web.models.Stores;

@Repository
public interface StoresRepository extends JpaRepository<Stores, Integer> {
    public Stores findByStoreId(Integer storeId);
    public Stores findByName(String name);
}