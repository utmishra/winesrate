package com.winesrate.web.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.winesrate.web.models.Country;

@Repository
public interface CountryRepository extends JpaRepository<Country, Integer> {

	public Country findByCountryName(String countryName);
}
