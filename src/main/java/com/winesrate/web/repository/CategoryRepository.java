package com.winesrate.web.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.winesrate.web.models.Category;

@Repository
public interface CategoryRepository extends JpaRepository<Category, Integer> {
    public Category findByCategoryId(Integer id);    
}