package com.winesrate.web.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.winesrate.web.models.Image;

@Repository
public interface ImageRepository extends JpaRepository<Image, Integer> {
    public Image findById(Integer id);
}