package com.winesrate.web.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.winesrate.web.models.Blog;

@Repository
public interface BlogRepository extends JpaRepository<Blog, Integer> {
    public Blog findByBlogId(Integer id);
    @Query(value = "SELECT * FROM blogs ORDER BY :sortBy :sortOrder LIMIT :limit OFFSET :offset", nativeQuery = true)
    public List<Blog> getAllBlogs(@Param("limit") Integer limit, @Param("offset") Integer offset, @Param("sortBy") String sortBy, @Param("sortOrder") String sortOrder);
    @Query(value = "SELECT * FROM blogs WHERE category_id = :categoryId ORDER BY :sortBy :sortOrder LIMIT :limit OFFSET :offset", nativeQuery = true)
    public List<Blog> findByCategoryId(@Param("categoryId") Integer categoryId, @Param("limit") Integer limit, @Param("offset") Integer offset, @Param("sortBy") String sortBy, @Param("sortOrder") String sortOrder);
}