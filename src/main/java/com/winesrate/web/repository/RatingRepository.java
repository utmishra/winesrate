package com.winesrate.web.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.winesrate.web.models.Rating;

@Repository
public interface RatingRepository extends JpaRepository<Rating, Integer> {
	@Query(value = "SELECT AVG(rating) FROM ratings WHERE wine_id = :wineId", nativeQuery = true)
	public Float getAvgRatingForWineId(@Param("wineId") Integer wineId);
}
