package com.winesrate.web.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.winesrate.web.models.Brands;

@Repository
public interface BrandRepository extends JpaRepository<Brands, Integer> {
    public Brands findByBrandId(Integer id);
    public Brands findByName(String name);
}