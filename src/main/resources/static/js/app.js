const app = (() => {
  // private functions
  
  let vintageHtml = "";
  let descHtml = "";
  const fetchSearchData = () => {
    let query = $('#querySearch').val();
    let pageNumber = $('#currentPageCount').val();
    let vintage = $('#vintageSearch').val();
    let continent = $('#continentSearch').val();
    $.ajax({
      url: "/wine/ajax",
      dataType: 'json',
      method: 'POST',
      data: {
        query: query,
        sortOrder: "ASC",
        sortBy: "price",
        pageNumber: pageNumber,
        size: "10",
        continent: continent,
        vintage: vintage
      }
    })
    .done( (data) => {
      document.getElementById('queryText').innerText = data['query'];
      parseData(data);
    })
    .error( (err) => {
      alert(err);
    });
  }

  const parseData = data => {
   vintageHtml = '';
   descHtml = '';
   let html = '';
   if(data.wines.length != 0) {
	    data.wines.map((val,key) => {
	      let newDescHtml = '';
	      let ratingHtml = '<td class="col-md-4 rating"><fieldset class="rating"><input type="radio" id="star5" name="rating" value="5"  ' + (val.rating == 5.0 ? 'checked' : '') + '/>';
	      ratingHtml += '<label class = "full" for="star5" title="Awesome - 5 stars"  ></label><input type="radio" id="star4half" name="rating" value="4 and a half" ' + (val.rating == 4.5 ? 'checked' : '') + ' />';
	      ratingHtml += '<label class="half" for="star4half" title="Pretty good - 4.5 stars"></label><input type="radio" id="star4" name="rating" value="4" ' + (val.rating == 4.0 ? 'checked' : '') + ' />';
	      ratingHtml += '<label class = "full" for="star4" title="Pretty good - 4 stars" ></label><input type="radio" id="star3half" name="rating" value="3 and a half" ' + (val.rating == 3.5 ? 'checked' : '') + ' />';
	      ratingHtml += '<label class="half" for="star3half" title="Meh - 3.5 stars" ></label><input type="radio" id="star3" name="rating" value="3" ' + (val.rating == 3.0 ? 'checked' : '') + ' />';
	      ratingHtml += '<label class = "full" for="star3" title="Meh - 3 stars" ></label><input type="radio" id="star2half" name="rating" value="2 and a half" ' + (val.rating == 2.5 ? 'checked' : '') + '/>';
	      ratingHtml += '<label class="half" for="star2half" title="Kinda bad - 2.5 stars"></label><input type="radio" id="star2" name="rating" value="2" ' + (val.rating == 2.0 ? 'checked' : '') + ' />';
	      ratingHtml += '<label class = "full" for="star2" title="Kinda bad - 2 stars" ></label><input type="radio" id="star1half" name="rating" value="1 and a half" ' + (val.rating == 1.5 ? 'checked' : '') + ' />';
	      ratingHtml += '<label class="half" for="star1half" title="Meh - 1.5 stars" ></label><input type="radio" id="star1" name="rating" value="1" ' + (val.rating == 1.0 ? 'checked' : '') + ' />';
	      ratingHtml += '<label class = "full" for="star1" title="Sucks big time - 1 star" ></label><input type="radio" id="starhalf" name="rating" value="half" ' + (val.rating == 0.5 ? 'checked' : '') + ' />';
	      window.ratingHtml = ratingHtml;
	      if(key == '0')
	    	  newDescHtml = '<table class="table table-striped table-bordered wine-table">';
	      else 
	    	  newDescHtml += '<tr>';            
	      newDescHtml += '<td class="col-md-3"><div class="result-title"><a href="' + val.url + '" class="clickWine" target="_blank">' + val.title + '</a></div>';
	      newDescHtml += '<div class="result-detail">';
	      if(val.vintage != null && val.vintage != '' && typeof val.vintage != 'undefined') {
	    	  newDescHtml += '<div class="detail-vintage"><img class="filter-icon png-icon" src="/images/hourglass.svg" alt="" /><span class="filter-icon">&nbsp;' + val.vintage + '</span>&nbsp;&nbsp;&nbsp;&nbsp;';
	      }
	      if(val.country != null && val.country != '' && typeof val.country != 'undefined') {
	    	  newDescHtml += '<img class="filter-icon png-icon" src="/images/home.svg" alt=""  /><span class="filter-icon">&nbsp;' + val.country + '</span>';  
	      }	      
	      if(val.appellation != null && val.appellation != '') {
	        newDescHtml += '&nbsp;&nbsp;&nbsp;&nbsp;<img class="filter-icon png-icon" src="/images/globe.png" alt=""/><span class="filter-icon">&nbsp;' + val.appellation +'</span>';
	      }
	      if(val.bottleSize != null && val.bottleSize!= '' && typeof val.bottleSize != 'undefined') {
	    	  newDescHtml += '</div><div class="detail-size"><img class="filter-icon png-icon" src="/images/ruler.svg" alt=""/><span class="filter-icon">&nbsp;' + val.bottleSize + '</span></div></div></td>';
	      }
	      else {
	    	  newDescHtml += '</div>';
	      }
	      newDescHtml += '</div>';
	      newDescHtml += '<td class="col-md-1"><div class="result-store"><a href="'  + val.store.storeURL + '" target="_blank"><img class="store-logo" src="' + val.store.logoUrl + '" /></a></div></td>';
	      newDescHtml += '<td class="col-md-2">$ ' + val.price + '</td>' + ratingHtml + '<td class="col-md-2"><a href="' + val.url + '" class="clickWine" target="_blank">Link</a></td></tr>';      
	      descHtml += newDescHtml;
	    });
	    paginationHtml = '<ul id="nav-tab">';
	    window.vintage = data.vintage;
	    var vintage = data.vintage != null ? data.vintage : '';
	    var continent = data.continent != null ? data.continent : '';
	    if(data.currentPageNumber >= 10) {
	    	paginationHtml += '<li><a class="pageLink" href="#" continent="' + continent + '" vintage="' + vintage + '" "sortBy="' + data.sortBy + '" sortOrder="' + data.sortOrder + '" pageNumber="' + (firstPageNav-10 > 1 ? firstPageNav - 10 : 0) + '"' + ' query="' + data.query + '">Previous</a></li>'; 
	    }
	    for(var i = data.firstPageNav; i <= data.lastPageNav; i++ ) {    	
	    	var activeText = i != data.currentPage ? '' : 'class="active"';    	
	    	paginationHtml += '<li ' + activeText + '><a href="#" continent="' + continent + '" vintage="' + vintage + '" data-val="' + (i-1) + '" sortBy="' + data.sortBy + '" sortOrder="' + data.sortOrder + '" pageNumber="' + i + '" query="' + data.query + '" class="pageLink">' + i + '</a></li>';
	    }
	    if(data.currentPage / 10 != data.totalPages/10 && data.totalPages > 10) {
	    	paginationHtml += '<li><a class="pageLink" href="#" continent="' + continent + '" vintage="' + vintage + '" sortBy="' + data.sortBy + '" sortOrder="' + data.sortOrder + '" pageNumber="' + (data.lastPageNav + 1) + '" query="' + data.query + '">Next</a></li>'
	    }
	    paginationHtml += '</ul>';	    
	    if(data.wines.length == 0)
	    	paginationHtml = '';
	    html = vintageHtml + descHtml + '</tbody></table>' + paginationHtml;
	    $('.filter-box').show();
   }
   else {
	   data.recommended.map((val,key) => {
	      let newDescHtml = '';
	      let ratingHtml = '<td class="col-md-4 rating"><fieldset class="rating"><input type="radio" id="star5" name="rating" value="5"  ' + (val.rating == 5.0 ? 'checked' : '') + '/>';
	      ratingHtml += '<label class = "full" for="star5" title="Awesome - 5 stars"  ></label><input type="radio" id="star4half" name="rating" value="4 and a half" ' + (val.rating == 4.5 ? 'checked' : '') + ' />';
	      ratingHtml += '<label class="half" for="star4half" title="Pretty good - 4.5 stars"></label><input type="radio" id="star4" name="rating" value="4" ' + (val.rating == 4.0 ? 'checked' : '') + ' />';
	      ratingHtml += '<label class = "full" for="star4" title="Pretty good - 4 stars" ></label><input type="radio" id="star3half" name="rating" value="3 and a half" ' + (val.rating == 3.5 ? 'checked' : '') + ' />';
	      ratingHtml += '<label class="half" for="star3half" title="Meh - 3.5 stars" ></label><input type="radio" id="star3" name="rating" value="3" ' + (val.rating == 3.0 ? 'checked' : '') + ' />';
	      ratingHtml += '<label class = "full" for="star3" title="Meh - 3 stars" ></label><input type="radio" id="star2half" name="rating" value="2 and a half" ' + (val.rating == 2.5 ? 'checked' : '') + '/>';
	      ratingHtml += '<label class="half" for="star2half" title="Kinda bad - 2.5 stars"></label><input type="radio" id="star2" name="rating" value="2" ' + (val.rating == 2.0 ? 'checked' : '') + ' />';
	      ratingHtml += '<label class = "full" for="star2" title="Kinda bad - 2 stars" ></label><input type="radio" id="star1half" name="rating" value="1 and a half" ' + (val.rating == 1.5 ? 'checked' : '') + ' />';
	      ratingHtml += '<label class="half" for="star1half" title="Meh - 1.5 stars" ></label><input type="radio" id="star1" name="rating" value="1" ' + (val.rating == 1.0 ? 'checked' : '') + ' />';
	      ratingHtml += '<label class = "full" for="star1" title="Sucks big time - 1 star" ></label><input type="radio" id="starhalf" name="rating" value="half" ' + (val.rating == 0.5 ? 'checked' : '') + ' />';
	      if(key == '0')
	    	  newDescHtml = '<table class="table table-striped table-bordered wine-table">';
	      else 
	    	  newDescHtml += '<tr>';            
	      newDescHtml += '<td class="col-md-3"><div class="result-title"><a href="' + val.url + '" class="clickWine" target="_blank">' + val.title + '</a></div>';
	      newDescHtml += '<div class="result-detail">';
	      if(val.vintage != null && val.vintage != '' && typeof val.vintage != 'undefined') {
	    	  newDescHtml += '<div class="detail-vintage"><img class="filter-icon png-icon" src="/images/hourglass.svg" alt="" /><span class="filter-icon">&nbsp;' + val.vintage + '</span>&nbsp;&nbsp;&nbsp;&nbsp;';
	      }
	      if(val.country != null && val.country != '' && typeof val.country != 'undefined') {
	    	  newDescHtml += '<img class="filter-icon png-icon" src="/images/home.svg" alt=""  /><span class="filter-icon">&nbsp;' + val.country + '</span>';  
	      }	      
	      if(val.appellation != null && val.appellation != '') {
	        newDescHtml += '&nbsp;&nbsp;&nbsp;&nbsp;<img class="filter-icon png-icon" src="/images/globe.png" alt=""/><span class="filter-icon">&nbsp;' + val.appellation +'</span>';
	      }		      
	      if(val.bottleSize != null && val.bottleSize!= '' && typeof val.bottleSize != 'undefined') {
	    	  newDescHtml += '</div><div class="detail-size"><img class="filter-icon png-icon" src="/images/ruler.svg" alt=""/><span class="filter-icon">&nbsp;' + val.bottleSize + '</span></div></div></td>';
	      }
	      else {
	    	  newDescHtml += '</div>';
	      }
	      newDescHtml += '</div>';
	      newDescHtml += '<td class="col-md-1"><div class="result-store"><a href="'  + val.store.storeURL + '" target="_blank"><img class="store-logo" src="' + val.store.logoUrl + '" /></a></div></td>';
	      newDescHtml += '<td class="col-md-2">$ ' + val.price + '</td>' + ratingHtml + '<td class="col-md-2"><a href="' + val.url + '" class="clickWine" target="_blank">Link</a></td></tr>';      
	      descHtml += newDescHtml;
	      $('.filter-box').hide();
	      document.getElementById('queryText').innerText = 'No results for ' + data['query'] + '. Take a look at recommended wines.';
	    });		    		    
	    vintageHtml + descHtml + '</tbody></table>';
	    html = vintageHtml + descHtml + '</tbody></table>';
   }
    $("#replaceableContent").html(html);
    $('.pageLink').click(function() {
		query = $(this).attr('query');
		vintage = $(this).attr('vintage');
		continent = $(this).attr('continent');
		pageNumber = $(this).attr('pagenumber')-1;
		if(vintage == 'All')
			vintage = '';
		$('#vintage').val(vintage);
		$('#pageNumber').val(pageNumber);
		$('#continentSearch').val(continent);
		$('#wineForm').submit();
	});
  }

  const detectChange = () => {
    $(".selectOptions").change(function() {
      fetchSearchData();
    });
    $(".nextPageCount").click(function(){
      let count = $(this).data('val');
      $('#currentPageCount').val(count);
      fetchSearchData();
    });
    $("#winePageSearch").submit((e) => {
      e.preventDefault();
      fetchSearchData();
    });
  }

  // public functions
  const init = () => {
    detectChange();
  }

  return {
    init: init
  }

})();

$(document).ready(function() {
  app.init();
});
